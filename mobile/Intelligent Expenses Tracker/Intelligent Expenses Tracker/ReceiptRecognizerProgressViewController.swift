//
//  ReceiptRecognizerProgressViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 02.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

class ReceiptRecognizerProgressViewController: UIViewController {
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var progressView: UIProgressView!

    var percentage: Int = 0 {
        didSet {
            if let percentageLabel = lblPercentage {
                let format = NSLocalizedString("%d%%", comment: "Format string for percents. %d - is a placeholder for percent number. %% - is a percent sign.")
                lblPercentage.text = String(format: format, percentage)
            }

            if let progressWidget = progressView {
                progressWidget.progress = Float(percentage) / 100.0
            }
        }
    }
}
