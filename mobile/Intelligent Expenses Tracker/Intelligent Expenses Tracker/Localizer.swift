//
//  Localizer.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 14.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import CoreData

public class Localizer {
    private let moc: NSManagedObjectContext

    public required init(managedObjectContext: NSManagedObjectContext) {
        moc = managedObjectContext
    }

    public func localize() {
        if currentLanguageCode != previousLanguageCode {
            // save current language code
            previousLanguageCode = currentLanguageCode
            updateOrCreateLocalizationData(currentLanguageCode)
        } else {
            // There is no need to update data that needs to be localized
        }
    }

    /// Gets or sets the language code, that was used to populate data storage with localized data during the last run
    /// of the application
    private var previousLanguageCode: String {
        get {
            // Get the name of the file which we use to store language code
            let fileURL = preferencesDirectory.URLByAppendingPathComponent("previousLanguageCode", isDirectory: false)

            var language = ""

            let fileManager = NSFileManager.defaultManager()
            if fileManager.fileExistsAtPath(fileURL.path!) {
                // Read language code
                language = String(contentsOfFile: fileURL.path!, encoding: NSUTF8StringEncoding, error: nil)!
            } else {
                // Do nothing.
            }

            return language
        }

        set {
            // Get the name of the file which we use to store language code
            let fileURL = preferencesDirectory.URLByAppendingPathComponent("previousLanguageCode", isDirectory: false)

            let fileManager = NSFileManager.defaultManager()
            // Write language code
            newValue.writeToFile(fileURL.path!, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
        }
    }

    /// Gets language code that is used by application right now
    private var currentLanguageCode: String {
        get {
            let languages = NSLocale.preferredLanguages() as [String]
            return languages[0]
        }
    }

    /// Gets URL of directory where application can store its internal preferences.
    private var preferencesDirectory: NSURL {
        get {
            let fileManager = NSFileManager.defaultManager()
            let applicationSupportDirectory = fileManager.URLForDirectory(.ApplicationSupportDirectory,
                inDomain: .UserDomainMask,
                appropriateForURL: nil,
                create: true,
                error: nil)!
            let bundleIdentifier = NSBundle.mainBundle().bundleIdentifier!
            let preferencesDirectory = applicationSupportDirectory.URLByAppendingPathComponent(bundleIdentifier, isDirectory: true)

            fileManager.createDirectoryAtURL(preferencesDirectory, withIntermediateDirectories: true, attributes: nil, error: nil)

            return preferencesDirectory
        }
    }

    /// Updates attributes of CoreData enitites using InitialData.plist and the given language.
    private func updateOrCreateLocalizationData(languageCode: String) {
        // Load property list with localization data
        let initialDataPath = NSBundle.mainBundle().pathForResource("InitialData", ofType: "plist")!
        let initialData = NSDictionary(contentsOfFile: initialDataPath)!

        // Get dictionary with entities for the given language
        var languageSpecificData = initialData.objectForKey(languageCode) as NSDictionary?

        if languageSpecificData == nil {
            // There is no localization data for the given language. Fallback to English
            languageSpecificData = initialData.objectForKey("en") as NSDictionary?
        }

        // Now we should check if provided data exist and if it is - update it. If it is not - update it
        //
        // The method I use to do this has pure performance, but it is simple for understanding, that' why I use it here
        //
        // In the real world applications, one should use another methods.
        //
        // There is a good optimization guide here:
        // https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/CoreData/Articles/cdImporting.html
        for entitiesData in languageSpecificData! {
            // Get the name of the CoreData entity
            let entityName = entitiesData.key as String

            // Get attributes
            let entities = entitiesData.value as [[String: String]]

            let predicate = NSPredicate(format: "id == $ID")!
            for entity in entities {
                let entityId = entity["id"]!

                // Construct request that will look for the entity with the given ID
                let request = NSFetchRequest(entityName: entityName)
                request.predicate = predicate.predicateWithSubstitutionVariables(["ID": entityId])
                request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
                request.resultType = .ManagedObjectResultType

                // Try to find entity with the given ID
                let objects = moc.executeFetchRequest(request, error: nil) as [NSManagedObject]?

                var entityToUpdate: NSManagedObject? = nil

                if objects != nil && objects!.count > 0 {
                    // Found. The code below will update it
                    entityToUpdate = objects![0]
                }

                if entityToUpdate == nil {
                    // There is no such data in the storage. Create it
                    entityToUpdate = NSEntityDescription.insertNewObjectForEntityForName(entityName,
                        inManagedObjectContext: moc) as? NSManagedObject
                }
                
                // Now we can fill entities attributes
                entityToUpdate?.setValuesForKeysWithDictionary(entity)
                
                moc.MR_saveToPersistentStoreAndWait()
            }
        }
    }
}
