//
//  Bar.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 01.03.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

@IBDesignable
public class Bar: UICollectionViewCell {
    private let NIBName = "Bar"
    private let FillSizeInPercents = "FillSizeInPercents"

    @IBOutlet
    private var view: UIView!

    @IBOutlet
    private weak var lblBarValue: UILabel!

    @IBOutlet
    private weak var lblBarLabel: UILabel!

    @IBOutlet
    private weak var bar: UIView!

    @IBOutlet
    private weak var fill: UIView!

    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadSubviewFromNib()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadSubviewFromNib()
    }

    private func loadSubviewFromNib() {
        let bundle = NSBundle(forClass: Bar.self)
        bundle.loadNibNamed(self.NIBName, owner: self, options: nil)

        self.addSubview(self.view)
        self.setupLayoutForSubview()
    }

    private func setupLayoutForSubview() {
        self.view.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[view]-0-|", options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics: nil, views: ["view": self.view]))

        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[view]-0-|", options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics: nil, views: ["view": self.view]))
    }

    @IBInspectable
    public var barBackgroundColor: UIColor? {
        get {
            return self.fill.backgroundColor
        }

        set {
            self.fill.backgroundColor = newValue
        }
    }

    @IBInspectable
    public var barValueText: String? {
        get {
            return self.lblBarValue.text
        }

        set {
            self.lblBarValue.text = newValue
        }
    }

    @IBInspectable
    public var barLabelText: String? {
        get {
            return self.lblBarLabel.text
        }

        set {
            self.lblBarLabel.text = newValue
        }
    }

    @IBInspectable
    public var percent: CGFloat = 0.5 {
        didSet {
            self.installBarConstraint()
        }
    }

    private func installBarConstraint() {
        if let oldConstraint = find(self.bar.constraints(), { ($0 as NSLayoutConstraint).identifier
            == self.FillSizeInPercents }) as? NSLayoutConstraint {
                NSLayoutConstraint.deactivateConstraints([oldConstraint])
        }

        let newConstraint = NSLayoutConstraint(
            item:       self.fill,
            attribute:  .Height,
            relatedBy:  .Equal,
            toItem:     self.bar,
            attribute:  .Height,
            multiplier: self.percent,
            constant:   0)
        newConstraint.identifier = self.FillSizeInPercents
        newConstraint.priority = 750
        NSLayoutConstraint.activateConstraints([newConstraint])
    }
}
