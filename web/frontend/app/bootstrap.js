/**
 * Created by Home-PC on 15.02.2015.
 */
define([
    'require',
    'angular',
    'app',
    'routes'
], function(require, ng){
    require(['domReady!'], function(document){
        ng.bootstrap(document, ['app']);
    });
});