/**
 * Created by Home-PC on 15.02.2015.
 */

define([
    'angular'
], function(ng){
    return ng.module('app.controllers', []);
});