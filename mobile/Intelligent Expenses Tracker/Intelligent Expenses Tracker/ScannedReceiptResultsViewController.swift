//
//  ScannedReceiptResultsViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 25.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

class ScannedReceiptResultsViewController: UIViewController {
    var scanResults: [ExpenseElement]?

    private var scanResultsController: ScannedReceiptResultsTableViewController!

    @IBAction func didTapSave(sender: UIBarButtonItem) {
        scanResultsController.save()
        view.window?.rootViewController?.dismissViewControllerAnimated(false, completion: nil)
    }

    @IBAction func didTapCancel(sender: UIBarButtonItem) {
        view.window?.rootViewController?.dismissViewControllerAnimated(false, completion: nil)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = SegueId(segue: segue)

        if segueId == .ShowScannedReceiptResultsTable {
            scanResultsController = segue.destinationViewController as ScannedReceiptResultsTableViewController
            scanResultsController.scanResults = scanResults
        }
    }
}
