//
//  ExpensesForCategoryViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 20.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit

class ExpensesForCategoryViewController: TableViewController, ExpenseDetailsControllerDelegate
{
    var category: ExpenseCategory!

    override func viewDidLoad() {
        title = self.category.name
        let predicate = NSPredicate(format: "category == %@", self.category)
        self.fetchedResultsController = Expense.MR_fetchAllSortedBy("date", ascending: false, withPredicate: predicate, groupBy: nil, delegate: self)
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellId = "Expense Cell"
        let cell = self.dequeueBrandedCellWithIdentifier(cellId) as ExpenseCell

        let expense = self.fetchedResultsController?.objectAtIndexPath(indexPath) as Expense
        cell.expense = expense
//        cell.textLabel?.text = expense.name
//        cell.detailTextLabel?.text = expense.localizedPrice

        return cell
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let expense = self.fetchedResultsController?.objectAtIndexPath(indexPath) as Expense
            let category = expense.category
            let context = self.fetchedResultsController?.managedObjectContext

            category.total = category.total.decimalNumberBySubtracting(expense.price)
            expense.MR_deleteInContext(context)
            context?.MR_saveToPersistentStoreAndWait()
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = SegueId(segue: segue)

        switch segueId {
        case .EditExistingExpense:
            let destinationController = segue.destinationViewController.childViewControllers!.first as ExpenseDetailsController
            destinationController.delegate = self
            let indexPath = tableView.indexPathForSelectedRow()
            let expense = self.fetchedResultsController?.objectAtIndexPath(indexPath!) as Expense
            destinationController.expense = expense
        default:
            ()
        }
    }

    func didSaveExpense(controller: ExpenseDetailsController) {
        controller.dismissViewControllerAnimated(true) {
            self.tableView.reloadData()
        }
    }

    func didCancelExpenseEditing(controller: ExpenseDetailsController) {
        controller.dismissViewControllerAnimated(true) {
        }
    }
}
