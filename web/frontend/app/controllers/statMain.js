/**
 * Created by Home-PC on 15.02.2015.
 */

define([
    './module',
    'jquery',
    'jquery-ui',
    'highstock'
], function(controllers, $){
    controllers.controller('statMain', ['mainExChart', 'mainIncChart', '$scope', 'ipCookie', function(mainExChart, mainIncChart, $scope, ipCookie){
        var chartZone = $('#oa-main-ex-charts'),
            incChartZone = $('#oa-main-inc-charts'),
            contentZone = $('#oa-main-ex-contents'),
            incContentZone = $('#oa-main-inc-contents'),
            reports = {
                r1: {
                    build: mainExChart.drawChartR1
                },
                r2: {
                    build: mainExChart.drawChartR2
                },
                r3: {
                    build: mainExChart.drawChartR3
                },
                r4: {
                    build: mainExChart.drawChartR4
                },
                r5: {
                    build: mainExChart.drawChartR5
                },
                r6: {
                    build: mainExChart.drawChartR6
                }
            },
            incomeReports = {
                ri1: {
                    build: mainIncChart.drawChartRI1
                },
                ri2: {
                    build: mainIncChart.drawChartRI2
                },
                ri3: {
                    build: mainIncChart.drawChartRI3
                }
            }

        $( "#tabs" ).tabs();

        contentZone.click(function(me){
            var target = me.target,
                main = target.tagName == 'LI' ? target : target.parentNode,
                reportName = main.getAttribute('data-report');

            reports[reportName].build(chartZone);
        });

        incContentZone.click(function(me){
            var target = me.target,
                main = target.tagName == 'LI' ? target : target.parentNode,
                reportName = main.getAttribute('data-report');

            incomeReports[reportName].build(incChartZone);
        });

        $('.oa-ex-contents-item').draggable({
            revert: 'valid'
        });

        $('.oa-inc-contents-item').draggable({
            revert: 'valid'
        });

        $('#oa-main-ex-charts').droppable({
            accept: '.oa-ex-contents-item',
            activeClass: 'oa-main-ex-charts-active',
            hoverClass: 'oa-main-ex-charts-hover',
            drop: function(event, ui){
                var elem = event.toElement;

                var report = (elem.tagName == 'LI' ? elem : elem.parentNode).getAttribute('data-report');

                reports[report].build(chartZone);
            }
        });

        $('#oa-main-inc-charts').droppable({
            accept: '.oa-inc-contents-item',
            activeClass: 'oa-main-ex-charts-active',
            hoverClass: 'oa-main-ex-charts-hover',
            drop: function(event, ui){
                var elem = event.toElement;

                var report = (elem.tagName == 'LI' ? elem : elem.parentNode).getAttribute('data-report');

                incomeReports[report].build(incChartZone);
            }
        });

        $scope.name = ipCookie('client').fname + ' ' + ipCookie('client').lname;
    }]);
});