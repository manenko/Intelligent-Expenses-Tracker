from rest_framework import generics, permissions
from django.contrib.auth.models import User
from rest_framework_bulk import ListBulkCreateUpdateDestroyAPIView
from api.filters import ExpenseFilter
from api.permissions import IsOwner
from api.serializers import ExpenseSerializer, ExpenseCategorySerializer, UserSerializer
from api.models import Expense, ExpenseCategory


class ExpenseList(ListBulkCreateUpdateDestroyAPIView):
    serializer_class = ExpenseSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner)
    filter_class = ExpenseFilter

    def get_queryset(self):
        user = self.request.user
        return Expense.objects.filter(owner=user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ExpenseDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ExpenseSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner)

    def get_queryset(self):
        user = self.request.user
        return Expense.objects.filter(owner=user)


class ExpenseCategoryList(generics.ListCreateAPIView):
    queryset = ExpenseCategory.objects.all()
    serializer_class = ExpenseCategorySerializer
    permission_classes = (permissions.IsAuthenticated, )


class ExpenseCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ExpenseCategory.objects.all()
    serializer_class = ExpenseCategorySerializer
    permission_classes = (permissions.IsAuthenticated, )


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, )


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, )
