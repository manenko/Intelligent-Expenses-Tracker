var express = require('express'),
    random = require('randomstring'),
    app = express(),
    mongojs = require('mongojs'),
    db = mongojs('mongodb://belka:Hi$$t0ry@ds045011.mongolab.com:045011/iet'),
    users = db.collection('users'),
    tokens = db.collection('tokens'),
    expenses = db.collection('expense'),
    incomes = db.collection('income'),
    expCat = db.collection('expenseCategory'),
    bodyParser = require('body-parser'),
    catList;

var user = require('user'),
    expense = require('expenses');

app.use(function(req, res, next) {
    req.rawBody = '';
    req.setEncoding('utf8');

    req.on('data', function(chunk) {
        req.rawBody += chunk;
    });

    req.on('end', function() {
        next();
    });
});

app.use('/user', user);
app.use('/expenses', expense);

app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname));
app.use(bodyParser());

app.post('/getToken', function(req, res){
    users.find(req.query, function(err, docs){
        var user = docs.length ? docs[0] : null;

        if(user){
            tokens.find({userId: docs[0]._id}, function(err, docs){
                var result = docs.length ? docs[0] : null;

                if(result){
                    if(!result.expired){
                        res.send({
                            success: true,
                            token: result.token,
                            fname: user.fname,
                            lname: user.lname
                        });
                    }
                } else {
                    var newToken = random.generate(10);

                    tokens.save({
                        userId: user._id,
                        token: newToken,
                        expired: false
                    }, function(){
                        res.send({
                            success: true,
                            token: newToken,
                            fname: user.fname,
                            lname: user.lname
                        });
                    })
                }
            });
        } else {
            res.send({
                success: false,
                error: 'User does not exist!'
            });
        }
    });
});

app.get('/checkToken', function(req, res) {
    res.send({
        expired: false
    })
});

app.post('/register', function(req, res){
    console.log(req.body);

    res.send({

    });
});

app.get('/getTopList', function(req, res){
    var topListByCategories = {};

    catList.forEach(function(item){
        topListByCategories[item.catId] = {
            name: item.name
        };

        topListByCategories[item.catId].data = null;
        getDataByCategory(item.catId, topListByCategories[item.catId]);
    });

    var timer = setInterval(function(){
        if(checkIfExist(topListByCategories)){
            res.send(topListByCategories);
            clearInterval(timer);
        }
    }, 1);
});

app.post('/saveExpense', function(req, res){
    var exp = req.body.expense;

    if(hasProperties(exp)){
        expenses.save(exp, function(err, docs){
            if(err){
                res.send({
                    success: false,
                    error: 'Something go wrong...'
                });
            }

            res.send({
                success: true
            });
        });
    } else {
        res.send({
            success: false,
            error: 'Bad request...'
        });
    }
});

app.post('/saveBulkExpenses', function(req, res){

    console.log(req);

    var getExpenses = JSON.parse(req.rawBody).expenses;

    var exp = getExpenses,
        len = exp.length,
        fake = [];


        for(var i = 0; i < len; i++){
            if(hasProperties(exp[i])){
                expenses.save(exp[i], function(err, docs){
                    if(err){
                        res.send({
                            success: false,
                            error: 'Something go wrong...'
                        });
                    }

                    fake.push(1);
                });
            } else{
                res.send({
                    success: false,
                    error: 'Bad request...'
                });
            }
        }

        var timer = setInterval(function(){
            if(fake.length = len){
                res.send({
                    success: true
                });
                clearInterval(timer);
            }
        }, 0);

});

app.get('/getAllIncome', function(req, res){
    incomes.aggregate([
        {
            $group: {
                _id: '$date',
                total: {$sum:'$income'}
            }
        },
        {
            $sort: { _id: 1 }
        }
    ], function(err, docs){
        res.send(docs);
    });
});

app.get('/getCompare', function(req, res){
    var sendData = {};

    sendData.data = [];

    incomes.aggregate([
        {
            $group: {
                _id: '$date',
                total: {$sum:'$income'}
            }
        },
        {
            $sort: { _id: 1 }
        }
    ], function(err, docs){
        var iarr = [],
            controlDate = '2013-01-01';

        controlDate = +(new Date(controlDate));

        docs.forEach(function(item){
            if(+(new Date(item._id)) > controlDate){
                iarr.push([
                    +(new Date(item._id)),
                    item.total
                ]);
            }
        });

        sendData.data.push({
            name: 'Income',
            data: iarr
        });

        expenses.aggregate([
            {
                $group: {
                    _id: '$date',
                    total: {$sum:'$price'}
                }
            },
            {
                $sort: { _id: 1 }
            }
        ], function(err, docs){
            var arr = [];

            docs.forEach(function(item){
                if(+(new Date(item._id)) > controlDate){
                    arr.push([
                        +(new Date(item._id)),
                        item.total
                    ]);
                }
            });

            sendData.data.push({
                name: 'Expenses',
                data: arr
            });

            res.send(sendData);
        });
    });
})

function checkIfExist(obj){
    for(var n in obj){
        if(obj.hasOwnProperty(n)){
            if(obj[n].data == undefined) return false
        }
    }

    return true;
}

function getDataByCategory(catID, obj){
    expenses.aggregate([
        {$match: {catId: catID}},
        {$sort: {price: -1}}
    ], function(err, docs){
        obj.data = docs.slice(0, 10);
    });
}

function getCategoryList(){
    expCat.find(function(err, docs){
        catList = docs;
    });
}

function hasProperties(obj){
    var exp = {
        catId: true,
        date: true,
        name: true,
        price: true
    };

    for(var n in exp){
        if(!obj[n]){
            return false;
        }
    }

    return true;
}

getCategoryList();

app.listen(app.get('port'), function() {
    console.log("Node app is running at localhost:" + app.get('port'));
});