//
//  ReceiptParser.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 07.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import Foundation

public struct Price: Printable {
    public let amount: NSDecimalNumber
    public let itemPrice: NSDecimalNumber
    public let totalPrice: NSDecimalNumber

    public var description: String {
        get {
            return "\(amount) X \(itemPrice) = \(totalPrice)"
        }
    }
}

public struct ExpenseElement: Printable {
    public let name: String
    public let price: Price

    public var description: String {
        get {
            return "\(name)\n\(price)"
        }
    }
}

public class ExpenseGenerator: GeneratorType {
    private var expenses: [String]
    private var currentIndex = 0

    public init(receipt: String) {
        expenses = splitReceiptText(receipt)
    }

    public func next() -> ExpenseElement? {
        var expenseElement: ExpenseElement?

        while currentIndex < expenses.count {
            let currentLine = expenses[currentIndex]
            let nextLineIndex = currentIndex + 1
            if nextLineIndex < expenses.count {
                let nextLine = expenses[nextLineIndex]
                if let price = parsePrice(nextLine) {
                    let name = parseName(currentLine)
                    expenseElement = ExpenseElement(name: name, price: price)
                    currentIndex += 2
                    break
                } else {
                    currentIndex++
                }
            } else {
                currentIndex = expenses.count
            }
        }

        return expenseElement
    }
}

public func parseReceipt(receipt: String) -> [ExpenseElement] {
    return [ExpenseElement](GeneratorOf(ExpenseGenerator(receipt: receipt)))
}

private func splitReceiptText(receiptText: String) -> [String] {
    return receiptText.componentsSeparatedByString("\n").filter() { $0.isEmpty == false }
}

private func parseName(line: String) -> String {
    // Pattern for names:
    // 1) Картофельное пюре 200
    // Арт.333 Пакет малий Novus Біо
    let pattern = "^.*\\d+\\)?\\s+(.+)"
    let matches = matchText(line, withPattern: pattern)

    var name = line

    if let match = matches.first {
        name = (line as NSString).substringWithRange(match.rangeAtIndex(1))
    }

    return name
}

private func parsePrice(text: String) -> Price? {
    var price: Price?

    let pattern = "^[^\\d]*(\\d+(?:\\.\\d+)?)[^\\d]*\\s+[XxХх]\\s+[^\\d]*(\\d+(?:\\.\\d+)?)[^\\d]+(\\d+(?:\\.\\d+)?)"
    let matches = matchText(text, withPattern: pattern)

    if let match = matches.first {
            let string = text as NSString
            let amountString = string.substringWithRange(match.rangeAtIndex(1))
            let itemPriceString = string.substringWithRange(match.rangeAtIndex(2))
            let totalPriceString = string.substringWithRange(match.rangeAtIndex(3))

            // Now we can convert strings to numbers
            let locale = NSLocale(localeIdentifier: "en")
            let amount = NSDecimalNumber(string: amountString, locale: locale)
            let itemPrice = NSDecimalNumber(string: itemPriceString, locale: locale)
            let totalPrice = NSDecimalNumber(string: totalPriceString, locale: locale)

            price = Price(amount: amount, itemPrice: itemPrice, totalPrice: totalPrice)
    }

    return price
}

private func matchText(text: String, withPattern pattern: String) -> [NSTextCheckingResult] {
    let regexp = NSRegularExpression(pattern: pattern, options: .CaseInsensitive, error: nil)
    let matches = regexp?.matchesInString(text, options: .allZeros, range: NSRange(location: 0, length: text.utf16Count)) as [NSTextCheckingResult]

    return matches
}
