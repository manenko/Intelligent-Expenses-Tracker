//
//  SampleBufferConverter.h
//  Intelligence
//
//  Created by Oleksandr Manenko on 25.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface ImageConverter : NSObject

+ (UIImage *)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer;

@end
