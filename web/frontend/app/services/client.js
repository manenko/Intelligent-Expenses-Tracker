/**
 * Created by Home-PC on 15.02.2015.
 */

define([
    './module',
    'jquery'
], function(services, $){
    services.factory('client', ['ipCookie', function(ipCookie){
        function _getStatus(){
            return ipCookie('status');
        }

        function _setStatus(status){
            ipCookie('status', status, {expires: 1});
        }

        function _getToken(){
            return ipCookie('token');
        }

        function _setToken(token){
            ipCookie('token', token, {expires: 1});
        }

        function _getPersonalData(){
            return ipCookie('client');
        }

        function _setPersonalData(data){
            ipCookie('client', {
                fname: data.fname,
                lname: data.lname
            }, {expires: 1});
        }

        function checkClient(){
            var token = _getToken(),
                status = _getStatus(),
                client  =_getPersonalData();

            if(token != undefined){
                $.ajax({
                    url: '/checkToken'
                }).then(function(response){
                    if(!response.expired){
                        var data = {
                            "fname": "Mykhailo",
                            "lname": "Mameko"
                        };

                        _setStatus('connected');
                        _setPersonalData(data);
                        window.location.replace('http://' + window.location.host + '/#/main')
                    }
                }, function(){

                });
            } else{
                _setStatus('unknown');
                _setPersonalData({
                    fname: '',
                    lname: ''
                })
            }
        }

        function get(name){
            return ipCookie(name);
        }

        function set(name, value){
            ipCookie(name, value, {expires: 1});
        }

        return {
            check: checkClient,
            get: get,
            set: set
        };
    }]);
});