//
//  UIViewControllerExtension.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 14.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit

public extension UIViewController {
    public func performSegueWithIdentifier(identifier: SegueId, sender: AnyObject?) {
        performSegueWithIdentifier(identifier.rawValue, sender: sender)
    }
}
