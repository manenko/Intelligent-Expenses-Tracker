# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('date', models.DateTimeField()),
                ('name', models.CharField(max_length=100)),
                ('price', models.DecimalField(decimal_places=2, max_digits=15)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExpenseCategory',
            fields=[
                ('id', models.CharField(primary_key=True, unique=True, serialize=False, max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('summary', models.TextField(blank=True, default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='expense',
            name='category',
            field=models.ForeignKey(to='api.ExpenseCategory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='expense',
            name='owner',
            field=models.ForeignKey(related_name='expenses', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
