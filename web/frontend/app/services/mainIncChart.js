/**
 * Created by Home-PC on 03.03.2015.
 */
define([
    './module',
    'jquery'
], function(services, $){
    services.factory('mainIncChart', [function(){
        var inc = {},
            chartZone = $('#oa-main-inc-charts');

        function hideLaunch(){
            chartZone.children().hide();
        }

        function showLaunch(){
            chartZone.children().show();
        }

        function dropCharts(){
            chartZone.children('div').remove();
        }

        inc.drawChartRI1 = function(drawTo){
            var container = document.createElement('div');

            hideLaunch();
            dropCharts();

            container.className = 'oa-chart-container';
            drawTo.append(container);

            $.ajax({
                url: '/getAllIncome',
                method: 'GET'
            }).then(function(response){
                var arr = [];

                response.forEach(function(item){
                    arr.push([
                        +(new Date(item._id)),
                        parseFloat(item.total.toFixed(2))
                    ]);
                });

                $(container).highcharts({
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: 'Загальні доходи'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Сума доходів (долл.)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">Рік: {point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}$</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    navigator: {
                        enabled: true
                    },
                    rangeSelector: {
                        enabled: true,
                        allButtonsEnabled: true,
                        inputEnabled: false
                    },
                    plotOptions:{
                        spline:{
                            marker: {
                                enabled: false
                            },
                            lineWidth: 1
                        }
                    },
                    series: [{
                        name: 'Динаміка доходів',
                        data: arr
                    }]
                });
            }, function(){

            });
        };

        inc.drawChartRI2 = function(drawTo){
            var container = document.createElement('div');

            hideLaunch();
            dropCharts();

            container.className = 'oa-chart-container';
            drawTo.append(container);

            $.ajax({
                url: '/getCompare',
                method: 'GET'
            }).then(function(response){
                console.log(response);

                $(container).highcharts({
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: 'Порівняння витрат та доходів'
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { // don't display the dummy year
                            month: '%e. %b',
                            year: '%b'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Суми в долл.'
                        },
                        min: 0
                    },
                    plotOptions: {
                        area: {
                            pointStart: 1940,
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    navigator: {
                        enabled: true
                    },
                    rangeSelector: {
                        enabled: true,
                        allButtonsEnabled: true,
                        inputEnabled: false
                    },
                    series: response.data
                });
            }, function(){

            });
        };

        inc.drawChartRI3 = function(drawTo){};

        return inc;
    }]);
});