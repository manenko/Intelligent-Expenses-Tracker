/**
 * Created by Home-PC on 26.02.2015.
 */
define([
    './module',
    'jquery'
], function(controllers, $){
    controllers.controller('regMain', ['client', function(client){
        var login = $('#oa-reg-email').val(),
            pass = $('#oa-reg-pass').val(),
            confPass = $('#oa-reg-conf-pass').val(),
            fname = $('#oa-reg-fname').val(),
            lname = $('#oa-reg-lname').val(),
            submitBtn = $('#oa-reg-submit');

        submitBtn.click(function(){
            console.log('click');
            $.ajax({
                url: '/register',
                method: 'post',
                data: {
                    login: login,
                    pass: pass,
                    fname: fname,
                    lname: lname
                }
            }).then(function(response){
                console.log(response);
            }, function(){

            });
        });
    }]);
});