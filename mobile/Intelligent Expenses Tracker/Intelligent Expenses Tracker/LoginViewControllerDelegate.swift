//
//  LoginViewControllerDelegate.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 13.03.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import Foundation

protocol LoginViewControllerDelegate: class {
    func didCancelSignIn(controller: LoginViewController)
    func didSignIn(controller: LoginViewController)
}
