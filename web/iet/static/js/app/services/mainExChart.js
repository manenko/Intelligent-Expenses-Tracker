define([
    './module',
    'jquery'
], function(services, $){
    services.factory('mainExChart', [function(){
        var exp = {},
            chartZone = $('#oa-main-ex-charts');

        function hideLaunch(){
            chartZone.children().hide();
        }

        function dropCharts(){
            chartZone.children('div').remove();
        }

        function drawList(data, to, isUpdate){
            if(!isUpdate){
                if($('.oa-data-detalised')){
                    $('.oa-data-detalised').remove();
                }
            }

            var listContainer = document.createElement('div'),
                counter = 1;

            listContainer.className = 'oa-data-detalised';

            if(to){
                $(to).append(listContainer);
            } else {
                chartZone.append(listContainer);
            }

            var table = document.createElement('table');

            $(listContainer).append(table);

            $(table).append('<thead>' +
            '<tr>' +
            '<td class="oa-thead-td">#</td>' +
            '<td class="oa-thead-td">Name</td>' +
            '<td class="oa-thead-td">Date</td>' +
            '<td class="oa-thead-td">Type</td>' +
            '<td class="oa-thead-td">Price</td>' +
            '</tr>' +
            '</thead>');

            var tbody = document.createElement('tbody');

            $(table).append(tbody);

            data.forEach(function(item){
                $(tbody).append('<tr>' +
                '<td class="oa-tbody-td">' + counter + '</td>' +
                '<td class="oa-tbody-td">' + item.name + '</td>' +
                '<td class="oa-tbody-td">' + item.date + '</td>' +
                '<td class="oa-tbody-td">' + item.category + '</td>' +
                '<td class="oa-tbody-td">' + item.price + '</td>' +
                '</tr>');

                counter++;
            });

            if(!to){
                $('.oa-data-detalised').dialog({
                    modal: true,
                    maxWidth: 450,
                    height: 500,
                    resizable: false,
                    draggable: false,
                    title: 'Your expenses of ' + data[0].date
                });
            }
        }

        exp.drawChartR1 = function(dm){
            var data = null,
                container = document.createElement('div'),
                year = dm.aggregateByYear();

            hideLaunch();
            dropCharts();

            container.className = 'oa-chart-container-main';

            chartZone.append(container);

            data = dm.aggregateByDate({
                type: 'year',
                start: year.categories[year.categories.length - 1],
                end: year.categories[year.categories.length - 1]
            });

            $(container).highcharts({
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Timeline of your expenses (UAH)'
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Total sum'
                    }
                },
                navigator: {
                    enabled: true
                },
                rangeSelector: {
                    enabled: true,
                    allButtonsEnabled: true,
                    inputEnabled: false
                },
                series: [{
                    name: 'Expenses',
                    data: data
                }],
                plotOptions: {
                    series: {
                        cursor: 'pointer'
                    },
                    spline: {
                        point: {
                            events: {
                                click: function(){
                                    var data = dm.getList({
                                        start: this.x,
                                        end: this.x
                                    });

                                    drawList(data);
                                }
                            }
                        }
                    }
                }
            });
        };

        exp.drawChartR2 = function(dm){
            var data = null,
                containerMain = document.createElement('div');

            hideLaunch();
            dropCharts();

            var listDetails = $('.oa-chart-details-list');

            if(listDetails){
                listDetails.remove();
            }

            data = dm.aggregateByYear();

            containerMain.className = 'oa-chart-container-main';

            chartZone.append(containerMain);

            $(containerMain).highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Your expenses by years (UAH)'
                },
                xAxis: {
                    categories: data.categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total sum'
                    }
                },
                series: [{
                    name: 'Expenses',
                    data: data.series
                }],
                plotOptions: {
                    column: {
                        point: {
                            events: {
                                click: function(){
                                    if($('.oa-chart-container')){
                                        $('.oa-chart-container').remove();
                                    }

                                    var that = this,
                                        dataMonth = dm.aggregateByMonth({
                                            type: 'year',
                                            start: that.category,
                                            end: that.category
                                        }),
                                        dataCat = dm.aggregateByCategories({
                                            type: 'year',
                                            start: that.category,
                                            end: that.category
                                        }),
                                        containerMonth = document.createElement('div'),
                                        containerCat = document.createElement('div'),
                                        containerCur = document.createElement('div');

                                    var list = document.createElement('ul'),
                                        container = document.createElement('div');

                                    container.className = 'oa-chart-container-main';

                                    chartZone.append(container);
                                    $(container).hide();

                                    list.className = 'oa-chart-container oa-chart-details-list';

                                    chartZone.append(list);

                                    containerMonth.className = containerCat.className = containerCur.className = 'oa-chart-container-mini';
                                    containerCur.className += ' oa-chart-container-mini-active';

                                    $(list).append(containerCur);
                                    $(list).append(containerMonth);
                                    $(list).append(containerCat);

                                    var img0 = document.createElement('img'),
                                        label0 = document.createElement('span');

                                    img0.src = '/static/images/column-chart.svg';
                                    label0.innerHTML = 'Your expenses by years (UAH) (Current main chart)';

                                    $(containerCur).append(img0);
                                    $(containerCur).append(label0);


                                    var img1 = document.createElement('img'),
                                        label1 = document.createElement('span');

                                    img1.src = '/static/images/column-chart.svg';
                                    label1.innerHTML = 'Your expenses of ' + that.category + ' year by month (UAH)';

                                    $(containerMonth).append(img1);
                                    $(containerMonth).append(label1);


                                    var img2 = document.createElement('img'),
                                        label2 = document.createElement('span');

                                    img2.src = '/static/images/pie-chart.svg';
                                    label2.innerHTML = 'Your expenses of ' + that.category + ' year by categories';

                                    $(containerCat).append(img2);
                                    $(containerCat).append(label2);


                                    $(containerMonth).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(containerMain).hide();
                                        $(container).show();
                                        $(container).highcharts({
                                            chart: {
                                                type: 'column'
                                            },
                                            title: {
                                                text: 'Your expenses of ' + that.category + ' year by month (UAH)'
                                            },
                                            xAxis: {
                                                categories: dataMonth.categories
                                            },
                                            yAxis: {
                                                min: 0,
                                                title: {
                                                    text: 'Total sum'
                                                }
                                            },
                                            series: [{
                                                name: 'Expenses',
                                                data: dataMonth.series
                                            }]
                                        });
                                    });

                                    $(containerCat).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(containerMain).hide();
                                        $(container).show();
                                        $(container).highcharts({
                                            chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false
                                            },
                                            title: {
                                                text: 'Your expenses of ' + that.category + ' year by categories'
                                            },
                                            series: [{
                                                type: 'pie',
                                                data: dataCat
                                            }]
                                        });
                                    });

                                    $(containerCur).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(container).hide();
                                        $(containerMain).show();
                                    });
                                }
                            }
                        }
                    }
                }
            });
        };

        exp.drawChartR3 = function(dm){
            var data = null,
                containerMain = document.createElement('div'),
                years = dm.aggregateByYear();

            hideLaunch();
            dropCharts();

            data = dm.aggregateByMonth({
                type: 'year',
                start: years.categories[years.categories.length - 2],
                end: years.categories[years.categories.length - 1]
            });

            containerMain.className = 'oa-chart-container-main';

            chartZone.append(containerMain);

            $(containerMain).highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Your expenses by months (UAH)'
                },
                xAxis: {
                    categories: data.categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total sum'
                    }
                },
                series: [{
                    name: 'Expenses',
                    data: data.series
                }],
                plotOptions: {
                    column: {
                        point: {
                            events: {
                                click: function(){
                                    if($('.oa-chart-container')){
                                        $('.oa-chart-container').remove();
                                    }

                                    var that = this,
                                        dataMonth = dm.aggregateByWeek({
                                            type: 'month',
                                            start: that.category,
                                            end: that.category
                                        }),
                                        dataCat = dm.aggregateByCategories({
                                            type: 'month',
                                            start: that.category,
                                            end: that.category
                                        }),
                                        containerMonth = document.createElement('div'),
                                        containerCat = document.createElement('div'),
                                        containerCur = document.createElement('div');

                                    var list = document.createElement('ul'),
                                        container = document.createElement('div');

                                    container.className = 'oa-chart-container-main';

                                    chartZone.append(container);
                                    $(container).hide();

                                    list.className = 'oa-chart-container oa-chart-details-list';

                                    chartZone.append(list);

                                    containerMonth.className = containerCat.className = containerCur.className = 'oa-chart-container-mini';
                                    containerCur.className += ' oa-chart-container-mini-active';

                                    $(list).append(containerCur);
                                    $(list).append(containerMonth);
                                    $(list).append(containerCat);

                                    var img0 = document.createElement('img'),
                                        label0 = document.createElement('span');

                                    img0.src = '/static/images/column-chart.svg';
                                    label0.innerHTML = 'Your expenses by months (UAH) (Current main chart)';

                                    $(containerCur).append(img0);
                                    $(containerCur).append(label0);

                                    var img1 = document.createElement('img'),
                                        label1 = document.createElement('span');

                                    img1.src = '/static/images/column-chart.svg';
                                    label1.innerHTML = 'Your expenses of ' + that.category + ' month by weeks (UAH)';

                                    $(containerMonth).append(img1);
                                    $(containerMonth).append(label1);

                                    var img2 = document.createElement('img'),
                                        label2 = document.createElement('span');

                                    img2.src = '/static/images/pie-chart.svg';
                                    label2.innerHTML = 'Your expenses by categories of ' + that.category + ' month';

                                    $(containerCat).append(img2);
                                    $(containerCat).append(label2);

                                    $(containerMonth).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(containerMain).hide();
                                        $(container).show();
                                        $(container).highcharts({
                                            chart: {
                                                type: 'column'
                                            },
                                            title: {
                                                text: 'Your expenses of ' + that.category + ' month by weeks (UAH)'
                                            },
                                            xAxis: {
                                                categories: dataMonth.categories
                                            },
                                            yAxis: {
                                                min: 0,
                                                title: {
                                                    text: 'Total sum'
                                                }
                                            },
                                            series: [{
                                                name: 'Expenses',
                                                data: dataMonth.series
                                            }]
                                        });
                                    });

                                    $(containerCat).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(containerMain).hide();
                                        $(container).show();
                                        $(container).highcharts({
                                            chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false
                                            },
                                            title: {
                                                text: 'Your expenses by categories of ' + that.category + ' month'
                                            },
                                            series: [{
                                                type: 'pie',
                                                data: dataCat
                                            }]
                                        });;
                                    });

                                    $(containerCur).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(container).hide();
                                        $(containerMain).show();
                                    });
                                }
                            }
                        }
                    }
                }
            });
        };

        exp.drawChartR4 = function(dm){
            var data = null,
                containerMain = document.createElement('div'),
                years = dm.aggregateByYear();

            hideLaunch();
            dropCharts();

            data = dm.aggregateByWeek({
                type: 'year',
                start: years.categories[years.categories.length - 1],
                end: years.categories[years.categories.length - 1]
            });

            containerMain.className = 'oa-chart-container-main';

            chartZone.append(containerMain);

            $(containerMain).highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Your expenses by weeks (UAH)'
                },
                xAxis: {
                    categories: data.categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total sum'
                    }
                },
                series: [{
                    name: 'Expenses',
                    data: data.series
                }],
                plotOptions: {
                    column: {
                        point: {
                            events: {
                                click: function(){
                                    if($('.oa-chart-container')){
                                        $('.oa-chart-container').remove();
                                    }

                                    var that = this,
                                        dataByDate = data = dm.aggregateByDate({
                                            type: 'week',
                                            start: that.category,
                                            end: that.category
                                        }),
                                        dataCat = dm.aggregateByCategories({
                                            type: 'week',
                                            start: that.category,
                                            end: that.category
                                        }),
                                        containerDate = document.createElement('div'),
                                        containerCat = document.createElement('div'),
                                        containerCur = document.createElement('div');

                                    var list = document.createElement('ul'),
                                        container = document.createElement('div');

                                    container.className = 'oa-chart-container-main';

                                    chartZone.append(container);
                                    $(container).hide();

                                    list.className = 'oa-chart-container oa-chart-details-list';

                                    chartZone.append(list);

                                    containerDate.className = containerCat.className = containerCur.className = 'oa-chart-container-mini';
                                    containerCur.className += ' oa-chart-container-mini-active';

                                    $(list).append(containerCur);
                                    $(list).append(containerDate);
                                    $(list).append(containerCat);

                                    var img0 = document.createElement('img'),
                                        label0 = document.createElement('span');

                                    img0.src = '/static/images/column-chart.svg';
                                    label0.innerHTML = 'Your expenses by weeks (UAH) (Current main chart)';

                                    $(containerCur).append(img0);
                                    $(containerCur).append(label0);

                                    var img1 = document.createElement('img'),
                                        label1 = document.createElement('span');

                                    img1.src = '/static/images/line-chart.svg';
                                    label1.innerHTML = 'Dynamic of your expenses of ' + that.category + ' week (UAH)';

                                    $(containerDate).append(img1);
                                    $(containerDate).append(label1);

                                    var img2 = document.createElement('img'),
                                        label2 = document.createElement('span');

                                    img2.src = '/static/images/pie-chart.svg';
                                    label2.innerHTML = 'Your expenses by categories of ' + that.category + ' week';

                                    $(containerCat).append(img2);
                                    $(containerCat).append(label2);

                                    $(containerDate).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(containerMain).hide();
                                        $(container).show();
                                        $(container).highcharts({
                                            chart: {
                                                type: 'spline'
                                            },
                                            title: {
                                                text: 'Dynamic of your expenses of ' + that.category + ' week (UAH)'
                                            },
                                            xAxis: {
                                                type: 'datetime'
                                            },
                                            yAxis: {
                                                title: {
                                                    text: 'Total sum'
                                                }
                                            },
                                            series: [{
                                                name: 'Expenses',
                                                data: dataByDate
                                            }],
                                            plotOptions: {
                                                spline: {
                                                    point: {
                                                        events: {
                                                            click: function(){
                                                                var data = dm.getList({
                                                                    start: this.x,
                                                                    end: this.x
                                                                });

                                                                drawList(data);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                    });

                                    $(containerCat).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(containerMain).hide();
                                        $(container).show();
                                        $(container).highcharts({
                                            chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false
                                            },
                                            title: {
                                                text: 'Your expenses by categories of ' + that.category + ' week'
                                            },
                                            series: [{
                                                type: 'pie',
                                                data: dataCat
                                            }]
                                        });
                                    });

                                    $(containerCur).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(container).hide();
                                        $(containerMain).show();
                                    });
                                }
                            }
                        }
                    }
                }
            });
        };

        exp.drawChartR5 = function(dm){
            var data = null,
                containerMain = document.createElement('div');

            hideLaunch();
            dropCharts();

            data = dm.aggregateByCategories();

            containerMain.className = 'oa-chart-container-main';

            chartZone.append(containerMain);

            $(containerMain).highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: 'Your expenses by categories'
                },
                series: [{
                    type: 'pie',
                    data: data
                }],
                plotOptions: {
                    pie: {
                        point: {
                            events: {
                                click: function(){
                                    if($('.oa-chart-container')){
                                        $('.oa-chart-container').remove();
                                    }

                                    var that = this;

                                    var data = dm.aggregateByDate(undefined, dm.getList(undefined, this.name)),
                                        containerDate = document.createElement('div'),
                                        containerCur = document.createElement('div');

                                    var list = document.createElement('ul'),
                                        container = document.createElement('div');

                                    container.className = 'oa-chart-container-main';

                                    chartZone.append(container);
                                    $(container).hide();

                                    list.className = 'oa-chart-container oa-chart-details-list';

                                    chartZone.append(list);

                                    containerDate.className = containerCur.className = 'oa-chart-container-mini';
                                    containerCur.className += ' oa-chart-container-mini-active';

                                    $(list).append(containerCur);
                                    $(list).append(containerDate);

                                    var img0 = document.createElement('img'),
                                        label0 = document.createElement('span');

                                    img0.src = '/static/images/pie-chart.svg';
                                    label0.innerHTML = 'Your expenses by categories (Current main chart)';

                                    $(containerCur).append(img0);
                                    $(containerCur).append(label0);

                                    var img1 = document.createElement('img'),
                                        label1 = document.createElement('span');

                                    img1.src = '/static/images/line-chart.svg';
                                    label1.innerHTML = 'Dynamic of your expenses on ' + that.name + ' (UAH)';

                                    $(containerDate).append(img1);
                                    $(containerDate).append(label1);

                                    $(containerDate).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(containerMain).hide();
                                        $(container).show();
                                        $(container).highcharts({
                                            chart: {
                                                type: 'spline'
                                            },
                                            title: {
                                                text: 'Dynamic of your expenses on ' + that.name + ' (UAH)'
                                            },
                                            xAxis: {
                                                type: 'datetime'
                                            },
                                            yAxis: {
                                                title: {
                                                    text: 'Total sum'
                                                }
                                            },
                                            series: [{
                                                name: 'Expenses',
                                                data: data
                                            }]
                                        });
                                    });

                                    $(containerCur).click(function(){
                                        $('.oa-chart-container-mini').removeClass('oa-chart-container-mini-active');
                                        $(this).addClass('oa-chart-container-mini-active');

                                        $(container).hide();
                                        $(containerMain).show();
                                    });
                                }
                            }
                        }
                    }
                }
            });
        };

        exp.drawChartR6 = function(dm){
            hideLaunch();
            dropCharts();

            var data = dm.topList();

            var accordion = document.createElement('div');

            accordion.className = 'oa-chart-container-main';

            chartZone.append(accordion);

            for(var n in data){
                if(data.hasOwnProperty(n)){
                    var h3 = document.createElement('h3'),
                        div = document.createElement('div');

                    h3.innerHTML = n;
                    div.className = 'oa-top-list-details';

                    $(accordion).append(h3);
                    $(accordion).append(div);

                    drawList(data[n], div, true);
                }
            }

            $(accordion).accordion();
        };

        exp.drawChartR7 = function(dm){
            hideLaunch();
            dropCharts();

            var container = document.createElement('div');

            container.className = 'oa-chart-container-main';

            chartZone.append(container);

            $.ajax({
                url: '/static/js/app/templates/customReport.html',
                method: 'GET'
            }).then(function(response){
                container.innerHTML = response;

                var selectYear = dm.aggregateByYear().categories,
                    selectCategory = dm.aggregateByCategories();

                var selYearStartEl = $('#oa-custom-start-year'),
                    selYearEndEl = $('#oa-custom-end-year'),

                    selMonthStartEl = $('#oa-custom-start-month'),
                    selMonthEndEl = $('#oa-custom-end-month'),

                    selWeekStartEl = $('#oa-custom-start-week'),
                    selWeekEndEl = $('#oa-custom-end-week'),

                    selDayStartEl = $('#oa-custom-start-day'),
                    selDayEndEl = $('#oa-custom-end-day'),

                    selCategoryEl = $('#oa-custom-category');

                selectYear.forEach(function(item){
                    selYearStartEl.append('<option>' + item + '</option>');
                    selYearEndEl.append('<option>' + item + '</option>');
                });

                selYearStartEl.change(function(){
                    var value = this.value,
                        selectMonth;

                    selMonthStartEl.children().remove();
                    selMonthStartEl.append('<option>- All -</option>');

                    selWeekStartEl.children().remove();
                    selWeekStartEl.append('<option>- All -</option>');

                    selDayStartEl.children().remove();
                    selDayStartEl.append('<option>- All -</option>');

                    if(value != '- All -'){
                        selectMonth = dm.aggregateByMonth({
                            type: 'year',
                            start: value,
                            end: value
                        }).categories;

                        selectMonth.forEach(function(item){
                            selMonthStartEl.append('<option>' + item + '</option>');
                        });
                    }

                    getCustomData();
                });

                selYearEndEl.change(function(){
                    var value = this.value,
                        selectMonth;

                    selMonthEndEl.children().remove();
                    selMonthEndEl.append('<option>- All -</option>');

                    selWeekEndEl.children().remove();
                    selWeekEndEl.append('<option>- All -</option>');

                    selDayEndEl.children().remove();
                    selDayEndEl.append('<option>- All -</option>');

                    if(value != '- All -'){
                        selectMonth = dm.aggregateByMonth({
                            type: 'year',
                            start: value,
                            end: value
                        }).categories;

                        selectMonth.forEach(function(item){
                            selMonthEndEl.append('<option>' + item + '</option>');
                        });
                    }

                    getCustomData();
                });

                selMonthStartEl.change(function(){
                    var value = this.value,
                        selectWeek;

                    selWeekStartEl.children().remove();
                    selWeekStartEl.append('<option>- All -</option>');

                    selDayStartEl.children().remove();
                    selDayStartEl.append('<option>- All -</option>');

                    if(value != '- All -'){
                        selectWeek = dm.aggregateByWeek({
                            type: 'month',
                            start: value,
                            end: value
                        }).categories;

                        selectWeek.forEach(function(item){
                            selWeekStartEl.append('<option>' + item + '</option>');
                        });
                    }

                    getCustomData();
                });

                selMonthEndEl.change(function(){
                    var value = this.value,
                        selectWeek;

                    selWeekEndEl.children().remove();
                    selWeekEndEl.append('<option>- All -</option>');

                    selDayEndEl.children().remove();
                    selDayEndEl.append('<option>- All -</option>');

                    if(value != '- All -'){
                        selectWeek = dm.aggregateByWeek({
                            type: 'month',
                            start: value,
                            end: value
                        }).categories;

                        selectWeek.forEach(function(item){
                            selWeekEndEl.append('<option>' + item + '</option>');
                        });
                    }

                    getCustomData();
                });

                selWeekStartEl.change(function(){
                    var value = this.value,
                        selectDay = dm.aggregateByDate({
                            type: 'week',
                            start: value,
                            end: value
                        });

                    selDayStartEl.children().remove();
                    selDayStartEl.append('<option>- All -</option>');

                    selectDay.forEach(function(item){
                        var day = new Date(item[0]);
                        selDayStartEl.append('<option>' + day.toISOString().substring(0, 10) + '</option>');
                    });

                    getCustomData();
                });

                selWeekEndEl.change(function(){
                    var value = this.value,
                        selectDay = dm.aggregateByDate({
                            type: 'week',
                            start: value,
                            end: value
                        });

                    selDayEndEl.children().remove();
                    selDayEndEl.append('<option>- All -</option>');

                    selectDay.forEach(function(item){
                        var day = new Date(item[0]);
                        selDayEndEl.append('<option>' + day.toISOString().substring(0, 10) + '</option>');
                    });

                    getCustomData();
                });

                selDayStartEl.change(function(){
                    getCustomData();
                });

                selDayEndEl.change(function(){
                    getCustomData();
                });

                selectCategory.forEach(function(item){
                    selCategoryEl.append('<option>' + item[0] + '</option>');
                });

                selCategoryEl.change(function(){
                    getCustomData();
                });

                function getCustomData(){
                    var options = {
                        years: {
                            start: selYearStartEl.val(),
                            end: selYearEndEl.val()
                        },
                        months: {
                            start: selMonthStartEl.val(),
                            end: selMonthEndEl.val()
                        },
                        weeks: {
                            start: selWeekStartEl.val(),
                            end: selWeekEndEl.val()
                        },
                        days: {
                            start: selDayStartEl.val(),
                            end: selDayEndEl.val()
                        },
                        category: selCategoryEl.val()
                        },
                        data = dm.custom(options);

                    drawList(data, container);
                }
            }, function(){
                console.log('Fail request!');
            });
        }

        return exp;
    }
])});