/**
 * Created by Home-PC on 15.02.2015.
 */
define([
    'app'
], function(app){
    return app.config(['$routeProvider', function($routeProvider){
        $routeProvider
            .when('/', {
                templateUrl: 'app/templates/startPage.html'
            })
            .when('/main', {
                templateUrl: 'app/templates/mainPage.html'
            })
            .when('/login', {
                templateUrl: 'app/templates/loginPage.html'
            })
            .when('/registration', {
                templateUrl: 'app/templates/regPage.html'
            })
            .when('/guide', {
                templateUrl: 'app/templates/guidePage.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);
});