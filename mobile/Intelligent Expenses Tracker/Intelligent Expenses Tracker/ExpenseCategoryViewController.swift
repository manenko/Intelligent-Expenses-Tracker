//
//  SelectExpenseCategoryViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 09.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit
import CoreData

class ExpenseCategoryViewController: TableViewController {
    var selectedCategory: ExpenseCategory?
    weak var delegate: ExpenseCategoryViewControllerDelegate?

    override func viewDidLoad() {
        setupFetchedResultsController()
        updateMarks()
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellId = "Expense Category Cell"
        let cell = dequeueBrandedCellWithIdentifier(cellId)
        let expenseCategory = fetchedResultsController!.objectAtIndexPath(indexPath) as ExpenseCategory
        cell.textLabel?.text = expenseCategory.name
        cell.detailTextLabel?.text = expenseCategory.summary

        return cell
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Select Category", comment: "")
    }

    override func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        return fetchedResultsController!.sectionForSectionIndexTitle(title, atIndex: index)
    }

    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]! {
        return fetchedResultsController?.sectionIndexTitles
    }

    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .None
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedCategory = (fetchedResultsController!.objectAtIndexPath(indexPath) as ExpenseCategory)

        tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .Checkmark
        delegate?.didSelectExpenseCategory(selectedCategory!)
    }

    private func updateMarks() {
        if let category = selectedCategory {
            if let indexPath = fetchedResultsController?.indexPathForObject(category) {
                tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .Checkmark
            }
        }
    }

    private func setupFetchedResultsController() {
        if let managedObjectContext = selectedCategory?.managedObjectContext {
            fetchedResultsController = ExpenseCategory.MR_fetchAllSortedBy("name", ascending: true, withPredicate: nil, groupBy: nil, delegate: self, inContext: managedObjectContext)
        } else {
            fetchedResultsController = ExpenseCategory.MR_fetchAllSortedBy("name", ascending: true, withPredicate: nil, groupBy: nil, delegate: self)
        }
        tableView.reloadData()
    }
}
