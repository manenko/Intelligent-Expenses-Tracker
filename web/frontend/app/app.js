/**
 * Created by Home-PC on 15.02.2015.
 */
define([
    'angular',
    'angular-route',
    'angular-ipcookie',
    'angular-cookies',
    'controllers/index',
    'services/index'
], function(ng){
    return ng.module('app', ['app.controllers', 'app.services', 'ngRoute', 'ngCookies', 'ipCookie']);
});