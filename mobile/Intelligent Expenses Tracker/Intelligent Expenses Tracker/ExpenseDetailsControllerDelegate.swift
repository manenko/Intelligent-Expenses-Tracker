//
//  ExpenseDetailsControllerDelegate.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 20.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import Foundation

protocol ExpenseDetailsControllerDelegate: class {
    func didSaveExpense(controller: ExpenseDetailsController)
    func didCancelExpenseEditing(controller: ExpenseDetailsController)
}