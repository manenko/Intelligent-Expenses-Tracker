//
//  BrandedTableViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 26.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

public class BrandedTableViewController: UITableViewController {
    private func applyBrandingToCell(cell: UITableViewCell) {
        if cell.selectionStyle != .None {
            let selectedBackgroundView = UIView()
            selectedBackgroundView.backgroundColor = UIColor.orangeColor()
            cell.selectedBackgroundView = selectedBackgroundView
        }
    }

    public func dequeueBrandedCellWithIdentifier(identifier: String) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier) as UITableViewCell
        applyBrandingToCell(cell)

        return cell
    }

    public override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAtIndexPath: indexPath)

        applyBrandingToCell(cell)

        return cell
    }
}
