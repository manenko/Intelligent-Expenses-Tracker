/**
 * Created by Home-PC on 11.03.2015.
 */
define([
    './module',
    'jquery'
], function(services, $){
    services.factory('DataModel', [function(){
        return function(url){
            function addFields(){
                var date = null,
                    year = null,
                    month = null,
                    week = null;

                this.data.forEach(function(item){
                    date = new Date(item.date);

                    year = date.Year();

                    item.year = year.getYear();
                    item.yearStart = year.getYearStart();
                    item.yearEnd = year.getYearEnd();

                    week = date.Week();

                    item.week = week.getString();
                    item.weekStart = week.getWeekStart();
                    item.weekEnd = week.getWeekEnd();

                    month = date.Month();

                    item.month = month.getString();
                    item.monthStart = month.getMonthStart();
                    item.monthEnd = month.getMonthEnd();

                    item.realDate = +date;
                });
            }

            function sortNumber(a, b){
                var a = +a.price,
                    b = +b.price;

                if(a > b){
                    return -1;
                } else if(a < b){
                    return 1;
                } else{
                    return 0;
                }
            }

            this.getList = function(period, category){
                if(category){
                    if(period){
                        var start = period.start,
                            end = period.end,
                            date,
                            arr = [];

                        this.data.forEach(function(item){
                            date = +(new Date(item.date));

                            if(date >= start && date <= end && item.category == category){
                                arr.push(item);
                            }
                        });

                        return arr;
                    } else {
                        var date,
                            arr = [];

                        this.data.forEach(function(item){
                            date = +(new Date(item.date));

                            if(item.category == category){
                                arr.push(item);
                            }
                        });

                        return arr;
                    };
                } else{
                    if(period){
                        var start = period.start,
                            end = period.end,
                            date,
                            arr = [];

                        this.data.forEach(function(item){
                            date = +(new Date(item.date));

                            if(date >= start && date <= end){
                                arr.push(item);
                            }
                        });

                        return arr;
                    } else return this.data;
                }
            };

            this.aggregateByDate = function(period, newData){
                var data = newData || this.data,
                    result = {},
                    arr = [],
                    start, end, date;

                if(period){
                    if(period.type == 'year'){
                        start = this.data.filter(function(item){
                            return item.year == period.start;
                        })[0].yearStart;

                        end = this.data.filter(function(item){
                            return item.year == period.end;
                        })[0].yearEnd;
                    } else if(period.type == 'month'){

                    } else if(period.type == 'week'){
                        start = this.data.filter(function(item){
                            return item.week == period.start;
                        })[0].weekStart;
                        end = this.data.filter(function(item){
                            return item.week == period.end;
                        })[0].weekEnd;
                    }
                } else {
                    start = +(new Date(1980, 0, 1));
                    end = +(new Date(2050, 0, 1));
                }

                data.forEach(function(item){
                    date = +(new Date(item.date));

                    if(date >= start && date <= end){
                        if(!result[item.date]){
                            result[item.date] = 0;
                        }

                        result[item.date] += (+item.price);
                    }
                });

                for(var n in result){
                    if(result.hasOwnProperty(n)){
                        arr.push([
                            +(new Date(n)),
                            +result[n].toFixed(2)
                        ]);
                    }
                }

                return arr;
            };

            this.aggregateByYear = function(period){
                var result = {},
                    arr = {};

                arr.categories = [];
                arr.series = [];

                var start = period ? period.start : 0,
                    end = period ? period.end : 9999;

                this.data.forEach(function(item){
                    if(item.year >= start && item.year <= end){
                        if(!result[item.year]){
                            result[item.year] = 0;
                        }

                        result[item.year] += (+item.price);
                    }
                });

                for(var n in result){
                    if(result.hasOwnProperty(n)){
                        arr.categories.push(n);
                        arr.series.push(+result[n].toFixed(2));
                    }
                }

                return arr;
            };

            this.aggregateByMonth = function(period){
                var result = {},
                    arr = {},
                    date = null,
                    start,
                    end;

                arr.categories = [];
                arr.series = [];

                if(period){
                    if(period.type == 'year'){
                        start = this.data.filter(function(item){ return item.year == period.start; })[0].yearStart;
                        end = this.data.filter(function(item){ return item.year == period.end; })[0].yearEnd;
                    } else if(period.type == 'month'){
                        start = (new Date((period ? period.start : '01-1980').stringMonthToInt())).Month().getMonthStart();
                        end = (new Date((period ? period.end : '01-2050').stringMonthToInt())).Month().getMonthEnd();
                    }
                } else {
                    start = new Date(1980, 0, 1);
                    end= new Date(2050, 0, 1);
                }

                this.data.forEach(function(item){
                    date = +(new Date(item.date));

                    if(date >= start && date <= end){
                        date = new Date(item.date);

                        if(!result[item.month]){
                            result[item.month] = 0;
                        }

                        result[item.month] += (+item.price);
                    }
                });

                for(var n in result){
                    if(result.hasOwnProperty(n)){
                        arr.categories.push(n);
                        arr.series.push(+result[n].toFixed(2));
                    }
                }

                return arr;
            };

            this.aggregateByWeek = function(period){
                var result = {},
                    arr = {},
                    start,
                    end;

                arr.categories = [];
                arr.series = [];

                if(period){
                    if(period.type == 'year'){
                        start = this.data.filter(function(item){
                            return item.year == period.start;
                        })[0].yearStart;

                        end = this.data.filter(function(item){
                            return item.year == period.end;
                        })[0].yearEnd;
                    } else if(period.type == 'month'){
                        start = this.data.filter(function(item){
                            return item.month == period.start;
                        })[0].monthStart;

                        end = this.data.filter(function(item){
                            return item.month == period.end;
                        })[0].monthEnd;
                    } else if(period.type == 'week'){
                        start = this.data.filter(function(item){
                            return item.week == period.start;
                        })[0].weekStart;
                        end = this.data.filter(function(item){
                            return item.week == period.end;
                        })[0].weekEnd;
                    }
                } else{
                    start = new Date(1980, 0, 1);
                    end= new Date(2050, 0, 1);
                }

                this.data.forEach(function(item){
                    if(period){
                        if(+(new Date(item.date)) >= +start && +(new Date(item.date)) <= end){
                            if(!result[item.week]){
                                result[item.week] = 0;
                            }

                            result[item.week] += (+item.price);
                        }
                    } else{
                        if(!result[item.week]){
                            result[item.week] = 0;
                        }

                        result[item.week] += (+item.price);
                    }
                });

                for(var n in result){
                    if(result.hasOwnProperty(n)){
                        arr.categories.push(n);
                        arr.series.push(+result[n].toFixed(2));
                    }
                }

                return arr;
            };

            this.aggregateByCategories = function(period){
                if(period){
                    if(period.type == 'year'){
                        var start = period ? period.start : 0,
                            end = period ? period.end : 9999,
                            result = {},
                            arr = [];

                        this.data.forEach(function(item){
                            if(item.year >= start && item.year <= end){
                                if(!result[item.category]){
                                    result[item.category] = 0;
                                }

                                result[item.category] += (+item.price);
                            }
                        });

                        for(var n in result){
                            if(result.hasOwnProperty(n)){
                                arr.push([
                                    n,
                                    result[n]
                                ]);
                            }
                        }

                        return arr;

                    } else if(period.type == 'month'){
                        var start = (new Date((period ? period.start : '01-1980').stringMonthToInt())).Month().getMonthStart(),
                            end = (new Date((period ? period.end : '01-2050').stringMonthToInt())).Month().getMonthEnd();

                        var result = {},
                            arr = [],
                            date;

                        this.data.forEach(function(item){
                            date = +(new Date(item.date));

                            if(date >= start && date <= end){
                                if(!result[item.category]){
                                    result[item.category] = 0;
                                }

                                result[item.category] += (+item.price);
                            }
                        });

                        for(var n in result){
                            if(result.hasOwnProperty(n)){
                                arr.push([
                                    n,
                                    result[n]
                                ]);
                            }
                        }

                        return arr;
                    } else if(period.type == 'week'){
                        var start = this.data.filter(function(item){
                                return item.week == period.start;
                            })[0].weekStart,
                            end = this.data.filter(function(item){
                                return item.week == period.end;
                            })[0].weekEnd,
                            result = {},
                            arr = [];

                        this.data.forEach(function(item){
                            date = +(new Date(item.date));

                            if(+(new Date(item.date)) >= +start && +(new Date(item.date)) <= end){
                                if(!result[item.category]){
                                    result[item.category] = 0;
                                }

                                result[item.category] += (+item.price);
                            }
                        });

                        for(var n in result){
                            if(result.hasOwnProperty(n)){
                                arr.push([
                                    n,
                                    result[n]
                                ]);
                            }
                        }

                        return arr;
                    }
                } else{
                    var result = {},
                        arr = [];

                    this.data.forEach(function(item){
                        if(!result[item.category]){
                            result[item.category] = 0;
                        }

                        result[item.category] += (+item.price);
                    });

                    for(var n in result){
                        if(result.hasOwnProperty(n)){
                            arr.push([
                                n,
                                +result[n].toFixed(2)
                            ]);
                        }
                    }

                    return arr;
                }
            };

            this.topList = function(count){
                var res = {};

                this.data.forEach(function(item){
                    if(!res[item.category]){
                        res[item.category] = [];
                    }

                    res[item.category].push(item);
                });

                for(var n in res){
                    if(res.hasOwnProperty(n)){
                        res[n].sort(sortNumber);
                        res[n] = res[n].slice(0, (count || 5));
                    }
                }

                return res;
            };

            this.custom = function(options){
                var result = this.data,
                    startDay = this.data.filter(function(item){
                        if(options.days.start != '- All -'){
                            return item.date == options.days.start;
                        } else {
                            return false;
                        }
                    }),
                    endDay = this.data.filter(function(item){
                        if(options.days.end != '- All -'){
                            return item.date == options.days.end;
                        } else {
                            return false;
                        }
                    }),
                    startWeek = this.data.filter(function(item){
                        if(options.weeks.start != '- All -'){
                            return item.week == options.weeks.start;
                        } else {
                            return false;
                        }
                    }),
                    endWeek = this.data.filter(function(item){
                        if(options.weeks.end != '- All -'){
                            return item.week == options.weeks.end;
                        } else {
                            return false;
                        }
                    }),
                    startMonth = this.data.filter(function(item){
                        if(options.months.start != '- All -'){
                            return item.month == options.months.start;
                        } else {
                            return false;
                        }
                    }),
                    endMonth = this.data.filter(function(item){
                        if(options.months.end != '- All -'){
                            return item.month == options.months.end;
                        } else {
                            return false;
                        }
                    }),
                    startYear = this.data.filter(function(item){
                        if(options.years.start != '- All -'){
                            return item.year == options.years.start;
                        } else {
                            return false;
                        }
                    }),
                    endYear = this.data.filter(function(item){
                        if(options.years.end != '- All -'){
                            return item.year == options.years.end;
                        } else {
                            return false;
                        }
                    });

                startDay = +(startDay.length == 0 ? 0 : startDay[0].realDate);
                endDay = +(endDay.length == 0 ? +(new Date(2050, 0, 1)) : endDay[0].realDate);

                startWeek = +(startWeek.length == 0 ? 0 : startWeek[0].weekStart);
                endWeek = +(endWeek.length == 0 ? +(new Date(2050, 0, 1)) : endWeek[0].weekEnd);

                startMonth = +(startMonth.length == 0 ? 0 : startMonth[0].monthStart);
                endMonth = +(endMonth.length == 0 ? +(new Date(2050, 0, 1)) : endMonth[0].monthEnd);

                startYear = +(startYear.length == 0 ? 0 : startYear[0].yearStart);
                endYear = +(endYear.length == 0 ? +(new Date(2050, 0, 1)) : endYear[0].yearEnd);

                //filter by day
                result = result.filter(function(item){
                    return ((item.realDate >= startDay) && (item.realDate <= endDay));
                });

                //filter by week
                result = result.filter(function(item){
                    return ((item.realDate >= startWeek) && (item.realDate <= endWeek));
                });

                //filter by month
                result = result.filter(function(item){
                    return ((item.realDate >= startMonth) && (item.realDate <= endMonth));
                });

                //filter by year
                result = result.filter(function(item){
                    return ((item.realDate >= startYear) && (item.realDate <= endYear));
                });

                //filter by category
                result = result.filter(function(item){
                    if(options.category == '- All -'){
                        return true;
                    } else {
                        return item.category == options.category;
                    }
                });

                return result;
            };

            function load(){
                var that = this;

                $.ajax({
                    url: url,
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json'
                    }
                }).then(function(response){
                    that.data = response;

                    that.data.forEach(function(item){
                        item.date = (item.date).toString().substring(0, 10);
                    });

                    addFields.call(that);

                    that.data.sort(function(a, b){
                        if(a.realDate > b.realDate){
                            return 1;
                        } else if (a.realDate < b.realDate){
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                });
            }

            load.call(this);
        };
    }]);
});