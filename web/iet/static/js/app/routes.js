define([
    'app'
], function(app){
    return app.config(['$routeProvider', function($routeProvider){
        $routeProvider
            .when('/', {
                templateUrl: '/static/js/app/templates/mainPage.html'
            })
            .when('/guide', {
                templateUrl: '/static/js/app/templates/guidePage.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);
});