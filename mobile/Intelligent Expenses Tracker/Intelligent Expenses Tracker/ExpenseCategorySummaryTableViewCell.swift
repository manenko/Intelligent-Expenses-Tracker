//
//  ExpenseCategorySummaryTableViewCell.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 12.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit

public class ExpenseCategorySummaryTableViewCell: UITableViewCell {
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblDate: UILabel!
    @IBOutlet private weak var lblTotal: UILabel!
    @IBOutlet private weak var img: UIImageView!

    private let emptyCategoryColor = UIColor.blackColor()
    private let nonEmptyCategoryColor = UIColor.redColor()

    public var title: String? {
        get {
            return lblTitle.text
        }

        set {
            lblTitle.text = newValue
        }
    }

    public var details: String? {
        get {
            return lblDate.text
        }

        set {
            lblDate.text = newValue
        }
    }

    public var total: NSDecimalNumber? {
        didSet {
            if let number = total {
                lblTotal.text = NSNumberFormatter.localizedStringFromNumber(number, numberStyle: .CurrencyStyle)
            }

            if NSDecimalNumber.zero().compare(total ?? NSDecimalNumber.zero()) == .OrderedSame {
                lblTotal.textColor = emptyCategoryColor
            } else {
                lblTotal.textColor = nonEmptyCategoryColor
            }
        }
    }

    public var categoryImage: UIImage? {
        get {
            return img.image
        }

        set {
            img.image = newValue
        }
    }
}
