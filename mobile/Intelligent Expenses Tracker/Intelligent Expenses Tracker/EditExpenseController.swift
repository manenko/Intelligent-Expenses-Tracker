//
//  AddExpenseManuallyController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 16.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit
import CoreData

private enum RowId: Int {
    init(indexPath: NSIndexPath) {
        assert(indexPath.section == 0, "Unknown section index")

        let rowId = RowId(rawValue: indexPath.row)
        self = rowId ?? RowId.Unknown
    }

    case Name       = 0
    case Cost       = 1
    case Category   = 2
    case Date       = 3
    case DatePicker = 4
    case Receipts   = 5
    case Unknown    = -1
}

class EditExpenseController: UITableViewController {
    // MARK: - IB Outlets

    @IBOutlet
    private var labels: [UILabel]!

    @IBOutlet
    private weak var txtName: UITextField!

    @IBOutlet
    private weak var txtCost: UITextField!

    @IBOutlet
    private weak var txtDate: UITextField!

    @IBOutlet
    private weak var datePicker: UIDatePicker!

    // MARK: - IB Actions

    @IBAction
    func didChangeDate() {
        txtDate.text = NSDateFormatter.localizedStringFromDate(datePicker.date, dateStyle: .ShortStyle, timeStyle: .ShortStyle)
    }

    @IBAction
    func didSingleTap(recognizer: UITapGestureRecognizer) {
        tableView.endEditing(true)
    }

    // MARK: - Properties

    private var datePickerHidden = false

    private lazy var managedObjectContext: NSManagedObjectContext = {
        return (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    }()

    var expense: Expense?

    override func viewDidLoad() {
        didChangeDate()
        makeLabelsToBeSameWidth(labels)
        toogleDatePicker()

        if expense == nil {
            let receipt = Receipt(managedObjectContext: managedObjectContext)
            expense = Expense(managedObjectContext: managedObjectContext)
            receipt.addExpensesObject(expense)
            expense!.receipt = receipt
        }
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return datePickerHidden && RowId(indexPath: indexPath) == .DatePicker ? 0 : super.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if RowId(indexPath: indexPath) == .Date {
            toogleDatePicker()
        }
    }

    private func toogleDatePicker() {
        datePickerHidden = !datePickerHidden

        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
