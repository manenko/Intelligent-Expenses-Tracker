// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ExpenseCategory.swift instead.

import CoreData

enum ExpenseCategoryAttributes: String {
    case id = "id"
    case lastUpdate = "lastUpdate"
    case name = "name"
    case summary = "summary"
    case total = "total"
}

enum ExpenseCategoryRelationships: String {
    case expenses = "expenses"
}

@objc
class _ExpenseCategory: NSManagedObject {

    // MARK: - Class methods

    class func entityName () -> String {
        return "ExpenseCategory"
    }

    class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }

    // MARK: - Life cycle methods

    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

    convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = _ExpenseCategory.entity(managedObjectContext)
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged
    var id: String

    // func validateId(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var lastUpdate: NSDate?

    // func validateLastUpdate(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var name: String

    // func validateName(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var summary: String?

    // func validateSummary(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var total: NSDecimalNumber

    // func validateTotal(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    // MARK: - Relationships

    @NSManaged
    var expenses: NSSet

}

extension _ExpenseCategory {

    func addExpenses(objects: NSSet) {
        let mutable = self.expenses.mutableCopy() as NSMutableSet
        mutable.unionSet(objects)
        self.expenses = mutable.copy() as NSSet
    }

    func removeExpenses(objects: NSSet) {
        let mutable = self.expenses.mutableCopy() as NSMutableSet
        mutable.minusSet(objects)
        self.expenses = mutable.copy() as NSSet
    }

    func addExpensesObject(value: Expense!) {
        let mutable = self.expenses.mutableCopy() as NSMutableSet
        mutable.addObject(value)
        self.expenses = mutable.copy() as NSSet
    }

    func removeExpensesObject(value: Expense!) {
        let mutable = self.expenses.mutableCopy() as NSMutableSet
        mutable.removeObject(value)
        self.expenses = mutable.copy() as NSSet
    }

}
