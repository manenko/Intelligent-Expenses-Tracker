//
//  ReportsViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 20.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit

class ReportsViewController: UICollectionViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var categories: [ExpenseCategory]!
    var maxTotal = NSDecimalNumber.zero()
    var backgroundColors = [
        UIColor(red: 0.58, green: 0.77, blue: 0.49, alpha: 1.0),
        UIColor(red:0.24, green:0.52, blue:0.78, alpha:1.0),
        UIColor.orangeColor(),
        UIColor(red:0.60, green:0.75, blue:1.00, alpha:1.0),
        UIColor(red:0.88, green:0.40, blue:0.40, alpha:1.0),
        UIColor(red:0.96, green:0.70, blue:0.42, alpha:1.0),
        UIColor(red:0.27, green:0.51, blue:0.56, alpha:1.0),
        UIColor(red:0.29, green:0.53, blue:0.91, alpha:1.0),
    ]

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.categories = filter(ExpenseCategory.MR_findAll() as [ExpenseCategory], { $0.total > NSDecimalNumber.zero() })
            .sorted { $0.total < $1.total }
        self.maxTotal = reduce(map(self.categories, { $0.total }), self.maxTotal, max)
        self.collectionView?.reloadData()
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Bar", forIndexPath: indexPath) as Bar

        let category = self.categories[indexPath.item]

        cell.barLabelText = category.name
        cell.barValueText = category.localizedTotal
        cell.percent = CGFloat(category.total.floatValue / self.maxTotal.floatValue)
        cell.barBackgroundColor = self.backgroundColors[indexPath.item]

        return cell
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let navigatiobBarHeight = self.navigationController?.navigationBar.frame.size.height
        let tabBarHeight = self.tabBarController?.tabBar.frame.size.height
        let statusBarHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        let totalHeight = collectionView.window!.frame.size.height
        let availableHeight =  totalHeight - (navigatiobBarHeight ?? 0) - (tabBarHeight ?? 0) - statusBarHeight

        let layout = collectionView.collectionViewLayout as UICollectionViewFlowLayout

        return CGSize(width: 100, height: availableHeight - layout.sectionInset.top - layout.sectionInset.bottom)
    }

    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView?.reloadData()
    }
}
