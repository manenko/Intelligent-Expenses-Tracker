//
//  Import.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 20.03.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import Foundation

func importExpenses(json: JSON, context: NSManagedObjectContext) {
    for (index: String, subJson: JSON) in json {
        let name = subJson["name"].string ?? "<unknown>"
        let price = subJson["price"].number ?? NSNumber(int: 0)
        let dateISO = subJson["date"].string
        let categoryId = subJson["category"].string

        let expense = Expense(managedObjectContext: context)
        if name.utf16Count > 24 {
            expense.name = name.substringToIndex(advance(name.startIndex, 25))
        } else {
            expense.name = name
        }
        expense.price = NSDecimalNumber(double: price.doubleValue)
        expense.category = ExpenseCategory.MR_findFirstByAttribute("id", withValue: categoryId, inContext: context) as ExpenseCategory

        if let date = dateISO {
            expense.date = NSDate.dateFromString(date) ?? NSDate()
        } else {
            expense.date = NSDate()
        }

        //expense.date = NSDate()

        expense.category.total = expense.category.total.decimalNumberByAdding(expense.price)
    }

    context.MR_saveToPersistentStoreAndWait()
}

func initializeWithTestData(context: NSManagedObjectContext) {
    let testDataPath = NSBundle.mainBundle().pathForResource("test_data", ofType: "json")!
    let testData = NSData(contentsOfFile: testDataPath)

    let json = JSON(data: testData!)
    Expense.MR_truncateAll()
    let categories = ExpenseCategory.MR_findAll() as [ExpenseCategory]
    for category in categories {
        category.total = NSDecimalNumber.zero()
    }

    importExpenses(json, context)
}
