//
//  CVImagePreprocessor.m
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 11.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

#import "CVImagePreprocessor.h"
//#import "Thresholder.h"
#import "CVMatConverter.h"

@implementation CVImagePreprocessor

+ (UIImage *)prepareImageForOCR:(UIImage *)image {
    // Find ROI
    // Remove dashes inside zeros (0)
    return [CVImagePreprocessor prepareImageForLiveView:image];
}

+ (UIImage *)prepareImageForLiveView:(UIImage *)image {
    cv::Mat mat = [CVMatConverter cvMatFromUIImage:image];
    cv::cvtColor(mat, mat, cv::COLOR_RGB2GRAY);
    cv::equalizeHist(mat, mat);

    cv::threshold(mat, mat, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);

    // Remove black holes in the image using an Ellipse kernel
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3));
    cv::morphologyEx(mat, mat, cv::MORPH_CLOSE, kernel);

    cv::blur(mat, mat, cv::Size(2, 2));

    return [CVMatConverter imageFromCvMat:mat];
}

@end
