//
//  ExpenseCategory.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 20.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import Foundation

@objc(ExpenseCategory)
class ExpenseCategory: _ExpenseCategory {
    class func defaultCategory() -> ExpenseCategory {
        return MR_findFirstByAttribute("id", withValue: "Miscellaneous") as ExpenseCategory
    }

    class func defaultCategoryInContext(context: NSManagedObjectContext) -> ExpenseCategory {
        return MR_findFirstByAttribute("id", withValue: "Miscellaneous", inContext: context) as ExpenseCategory
    }

    var localizedTotal: String {
        get {
            return NSNumberFormatter.localizedStringFromNumber(total, numberStyle: .CurrencyStyle)
        }

        set {
            total = NSDecimalNumber(string: newValue, locale: NSLocale.currentLocale())
        }
    }
}
