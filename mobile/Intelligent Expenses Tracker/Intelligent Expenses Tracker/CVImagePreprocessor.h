//
//  CVImagePreprocessor.h
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 11.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVImagePreprocessor : NSObject

+ (UIImage *)prepareImageForOCR:(UIImage *)image;
+ (UIImage *)prepareImageForLiveView:(UIImage *)image;

@end
