//
//  AddExpenseManuallyController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 16.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit
import CoreData

private enum RowId: Int {
    init(indexPath: NSIndexPath) {
        assert(indexPath.section == 0, "Unknown section index")

        let rowId = RowId(rawValue: indexPath.row)
        self = rowId ?? RowId.Unknown
    }

    case Name       = 0
    case Cost       = 1
    case Category   = 2
    case Date       = 3
    case DatePicker = 4
    case Receipts   = 5
    case Unknown    = -1
}

class ExpenseDetailsController: BrandedTableViewController, ExpenseCategoryViewControllerDelegate {
    // MARK: - IB Outlets

    @IBOutlet
    private var labels: [UILabel]!

    @IBOutlet
    private weak var txtName: UITextField!

    @IBOutlet
    private weak var txtCost: UITextField!

    @IBOutlet
    private weak var txtDate: UITextField!

    @IBOutlet
    private weak var datePicker: UIDatePicker!

    @IBOutlet
    private weak var txtCategory: UITextField!

    @IBOutlet
    private weak var btnLeft: UIBarButtonItem!

    @IBOutlet
    private weak var btnRight: UIBarButtonItem!

    // MARK: - IB Actions

    @IBAction
    func didChangeDate() {
        txtDate.text = NSDateFormatter.localizedStringFromDate(datePicker.date, dateStyle: .ShortStyle, timeStyle: .ShortStyle)
    }

    @IBAction
    func didSingleTap(recognizer: UITapGestureRecognizer) {
        tableView.endEditing(true)
    }

    @IBAction
    func didTapDoneButton(sender: UIBarButtonItem) {
        let oldPrice = expense?.price
        let oldCategory = expense?.category
        if expense == nil {
            expense = (Expense.MR_createEntity() as Expense)
        } else {
            expense!.category.removeExpensesObject(expense)
            println("Used existing expense")
        }

        expense!.name = txtName.text
        expense!.localizedPrice = txtCost.text
        expense!.date = datePicker.date

        category!.total = expense!.price.decimalNumberByAdding(category!.total)

        if let price = oldPrice {
            if let cat = oldCategory {
                cat.total = cat.total.decimalNumberBySubtracting(price)
            }
        }

        category!.lastUpdate = expense!.date
        category!.addExpensesObject(expense!)
        expense!.category = category!

        if shouldSave {
            expense!.managedObjectContext!.MR_saveToPersistentStoreAndWait()
            println("Saved expense to persistent store")
        }

        delegate?.didSaveExpense(self)
    }

    @IBAction
    func didTapCancelButton(sender: UIBarButtonItem) {
        delegate?.didCancelExpenseEditing(self)
    }

    @IBAction
    func didChangeExpenseName(sender: UITextField) {
        updateRightButtonState()
    }

    @IBAction
    func didChangeExpenseCost(sender: UITextField) {
        updateRightButtonState()
    }

    // MARK: - Properties

    private var datePickerHidden = false
    private var category: ExpenseCategory? {
        didSet {
            txtCategory.text = category == nil ? nil : category!.name
        }
    }

    var expense: Expense?
    weak var delegate: ExpenseDetailsControllerDelegate?
    var shouldSave = true

    func didSelectExpenseCategory(selectedCategory: ExpenseCategory) {
        category = selectedCategory
        navigationController?.popViewControllerAnimated(true)
        updateRightButtonState()
    }

    private func updateRightButtonState() {
        let name = txtName.text ?? ""
        let costNumber = NSDecimalNumber(string: txtCost.text, locale: NSLocale.currentLocale())

        btnRight.enabled = !(name.isEmpty || costNumber.isEqualToNumber(NSDecimalNumber.notANumber()) || costNumber.isEqualToNumber(NSDecimalNumber.zero()) ||
            category == nil)
    }

    override func viewDidLoad() {
        didChangeDate()
        makeLabelsToHaveSameWidth(self.labels)
        toogleDatePicker()

        if let expenseDetails = expense {
            txtName.text    = expenseDetails.name
            txtCost.text    = NSNumberFormatter.localizedStringFromNumber(expenseDetails.price, numberStyle: .DecimalStyle)
            datePicker.date = expenseDetails.date
            category        = expenseDetails.category
            self.didChangeDate()
        } else {
            //category = ExpenseCategory.defaultCategory()
        }

        updateRightButtonState()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = SegueId(segue: segue)

        switch segueId {
        case .SelectExpenseCategory:
            let controller = segue.destinationViewController as ExpenseCategoryViewController
            controller.selectedCategory = category
            controller.delegate = self
        default:
            assertionFailure("Unknown Segue")
        }
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return datePickerHidden && RowId(indexPath: indexPath) == .DatePicker ? 0 : super.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if RowId(indexPath: indexPath) == .Date {
            toogleDatePicker()
        }
    }

    private func toogleDatePicker() {
        datePickerHidden = !datePickerHidden

        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
