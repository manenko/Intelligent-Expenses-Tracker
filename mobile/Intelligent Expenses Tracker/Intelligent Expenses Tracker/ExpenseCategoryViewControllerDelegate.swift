//
//  ExpenseCategoryViewControllerDelegate.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 19.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import Foundation

protocol ExpenseCategoryViewControllerDelegate: class {
    func didSelectExpenseCategory(selectedCategory: ExpenseCategory)
}