/**
 * Created by Home-PC on 26.02.2015.
 */
define([
    './module',
    'jquery'
], function(controllers, $){
    controllers.controller('loginMain', ['client', function(client){
        var login = $('#oa-login-email'),
            pass = $('#oa-login-pass');
            submitBtn = $('#oa-login-submit');

        submitBtn.click(function(){
            var sendData = {
                login: login.val(),
                pass: pass.val()
            };

            $.ajax({
                method: 'POST',
                url: '/getToken',
                data: sendData
            }).then(function(response){
                if(response.success){
                    client.set('client', {
                        fname: response.fname,
                        lname: response.lname
                    });

                    client.set('token', response.token);

                    window.location.replace('http://' + window.location.host + '/#/main');
                } else {
                    alert(response.error);
                }
            }, function(){

            });
        });
    }]);
});