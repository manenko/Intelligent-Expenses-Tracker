//
//  ReceiptRecognizerViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 29.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit
import TesseractOCR

class ReceiptRecognizerViewController: UIViewController, G8TesseractDelegate {
    lazy var recognizer: G8Tesseract = {
        return G8Tesseract(language: "eng+ukr")
    }()

    var progressController: ReceiptRecognizerProgressViewController!

    var receiptImage: UIImage? {
        didSet {
            imageView?.image = receiptImage
        }
    }

    private var shouldCancel = false
    private var expenses = [ExpenseElement]()

    func shouldCancelImageRecognitionForTesseract(tesseract: G8Tesseract!) -> Bool {
        dispatch_async(dispatch_get_main_queue()) {
            self.progressController.percentage = Int(tesseract.progress)
        }
        return shouldCancel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView?.image = receiptImage

        setupRecognizer()
        recognize()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = SegueId(segue: segue)

        switch segueId {
        case .ReceiptRecognizerProgress:
            progressController = segue.destinationViewController as ReceiptRecognizerProgressViewController
        case .ShowScannedReceiptResults:
            let controller = segue.destinationViewController as ScannedReceiptResultsViewController
            controller.scanResults = expenses
        default:
            ()
        }
    }

    private func recognize() {
        expenses = [ExpenseElement]()
        if let image = receiptImage {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let preprocessedImage = CVImagePreprocessor.prepareImageForOCR(image)
                dispatch_async(dispatch_get_main_queue()) {
                    self.imageView.image = self.receiptImage//preprocessedImage
                }

                self.recognizer.image = preprocessedImage
                var recognized = false
//                if self.recognizer.recognize() {
//                    self.expenses = parseReceipt(self.recognizer.recognizedText)
//                    //self.drawBoxes(self.recognizer.characterBoxes!)
//                    recognized = self.expenses.count > 0
//                } else {
//                    recognized = false
//                }

                self.recognizer.recognize()
                self.expenses = self.parseTestExpenses()
                recognized = true

                dispatch_after(0, dispatch_get_main_queue()) {
                    if recognized {
                        self.performSegueWithIdentifier(.ShowScannedReceiptResults, sender: self)
                    } else {
                        let alertTitle = NSLocalizedString("Error", comment: "")
                        let alertMessage = NSLocalizedString("Can't recognize receipt. Please try to scan it again or add expenses manually.", comment: "")
                        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .Alert)
                        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .Default) {
                            action in
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                        alertController.addAction(okAction)

                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
    }

    private func drawBoxes(boxes: [AnyObject]) {
        var boxInfo = [(String, CGRect)]()
        for box in boxes {
            boxInfo.append(readBoxInfo(box))
        }

        // Draw boxes over the image
        dispatch_async(dispatch_get_main_queue()) {
            if let image = self.imageView.image {
                // Image exist. Draw boxes
                UIGraphicsBeginImageContext(image.size)
                image.drawAtPoint(CGPoint.zeroPoint)

                let gc = UIGraphicsGetCurrentContext()
                UIColor.redColor().setStroke()

                for (text, box) in boxInfo {
                    CGContextStrokeRectWithWidth(gc, box, 2)
                }

                let imageWithBoxes = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()

                self.imageView.image = imageWithBoxes
            }
        }
    }

    private func readBoxInfo(obj: AnyObject) -> (String, CGRect) {
        let dict = obj as NSDictionary
        let value = dict.objectForKey("box") as NSValue

        let text = dict.objectForKey("text") as String
        let box = value.CGRectValue()

        return (text, box)
    }

    private func setupRecognizer() {
        let allowedCharacters =
            "АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюя" +
            " ,.№=*%:\"'-" +
            "0123456789" +
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        recognizer.setVariableValue(allowedCharacters, forKey: "tessedit_char_whitelist")
        recognizer.delegate = self
    }

    @IBOutlet
    weak var imageView: UIImageView!

    @IBAction
    func didTapCancel(sender: UIBarButtonItem) {
        shouldCancel = true
        dispatch_after(0, dispatch_get_main_queue()) {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }

    private func parseTestExpenses() -> [ExpenseElement] {
        let expenses = parseReceipt(testExpenses)

        return expenses
    }

    private let testExpenses = "\n".join([
            "Пакет великий Novus Bio",  "1.000 X 1.99 = 1.99 A",
            "Огірки з лис.дуба і виш.", "1.000 Х 42.99 = 42.99 А",
            "Скумбрія б/г х/к велика",  "0.392 Х 234.99 = 92.12 А",
            "Молоко 3.2% Яготин 1.5к",  "1.000 Х 21.12 = 21.12 А",
            "Петрушка кг",              "0.040 Х 159.99 = 6.40 А",
            "Хліб Литовський кг",       "0.438 Х 15.99 = 7.00 А",
            "Ананас шматочками Дольче", "1.000 Х 37.99 = 37.99 А",
            "Сир Бринза 45% Добряна в", "0.354 Х 85.07 = 30.11 А",
            "Сир Старий Голландець Ко", "0.206 Х 106.39 = 21.92 А",
            "Сир Король Артур Добряна", "0.266 Х 99.66 = 26.51 А",
            "Сир Король Артур Добряна", "0.282 Х 99.66 = 28.10 А",
            "Сир Швейцарія Елітсир 45", "0.240 Х 110.40 = 26.50 А",
            "Сир Gran Bavarese Bergad", "1.000 Х 59.99 = 59.99 А",
            "Ковбаса Тірол.Алан в/к в", "0.280 Х 149.99 = 42.00 А",
            "Сир Моцарелла черрі 45%",  "0.348 Х 121.09 = 42.14 А",
            "Презервативи Dolphi Ultr", "1.000 х 114.99 = 114.99 А",
            "Ковбаса ялов.Ятрань н/к",  "0.458 Х 174.99 = 80.15 А",
            "Ковбаса Сер.Укрпромпоста", "0.392 Х 103.99 = 40.76 А"
        ])

    class func testCategories() -> [String] {
        return [
            "Miscellaneous",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Food",
            "Personal Care",
            "Food",
            "Food"
        ]
    }
}
