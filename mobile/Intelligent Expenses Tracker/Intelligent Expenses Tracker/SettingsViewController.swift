//
//  SettingsViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 21.02.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

class SettingsViewController: BrandedTableViewController, LoginViewControllerDelegate {

    @IBOutlet weak var ietSyncSwitch: UISwitch!
    private var client: Client!

    @IBAction func didChangeIETSync(sender: UISwitch) {
        println("didChangeIETSync: \(sender.on)")
        if sender.on {
            // Present login view
            self.performSegueWithIdentifier(.Login, sender: self)
        } else {
            self.client.authToken = nil
        }
    }

    override func viewDidLoad() {
        self.client = Client()
        self.ietSyncSwitch.on = self.client.authToken != nil
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = SegueId(segue: segue)

        switch segueId {
        case .Login:
            let controller = segue.destinationViewController as LoginViewController
            controller.delegate = self
        default:
            ()
        }
    }

    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func didCancelSignIn(controller: LoginViewController) {
        controller.dismissViewControllerAnimated(true) {
            self.ietSyncSwitch.setOn(false, animated: true)
        }
    }

    func didSignIn(controller: LoginViewController) {
        controller.dismissViewControllerAnimated(true) {
        }
    }
}
