//
//  ExpenseCell.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 22.03.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

class ExpenseCell: UITableViewCell {
    private let NIBName = "ExpenseCell"

    @IBOutlet weak var expenseDateLabel: UILabel!
    @IBOutlet var view: UIView!
    @IBOutlet weak var expenseCategoryLabel: UILabel!
    @IBOutlet weak var expenseNameLabel: UILabel!
    @IBOutlet weak var expensePriceLabel: UILabel!

    var expense: Expense? {
        didSet {
            expenseCategoryLabel?.text = expense?.category.name
            expenseNameLabel?.text = expense?.name
            expensePriceLabel?.text = expense?.localizedPrice

            if let date = expense?.date {
                expenseDateLabel?.text = NSDateFormatter.localizedStringFromDate(
                    date,
                    dateStyle: .ShortStyle,
                    timeStyle: .NoStyle)
            } else {
                expenseDateLabel?.text = nil
            }
        }
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadSubviewFromNib()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadSubviewFromNib()
    }

    private func loadSubviewFromNib() {
        let bundle = NSBundle(forClass: Bar.self)
        bundle.loadNibNamed(self.NIBName, owner: self, options: nil)

        self.addSubview(self.view)
        self.setupLayoutForSubview()
    }

    private func setupLayoutForSubview() {
        self.view.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[view]-0-|", options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics: nil, views: ["view": self.view]))

        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[view]-0-|", options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics: nil, views: ["view": self.view]))
    }

}
