//
//  TableViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 08.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit
import CoreData

public class TableViewController: BrandedTableViewController, NSFetchedResultsControllerDelegate {
    public var fetchedResultsController: NSFetchedResultsController? {
        didSet {
            if oldValue != fetchedResultsController {
                oldValue?.delegate = nil
            }
        }
    }

    // MARK: - UITableViewDataSource
    public override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 1
    }

    public override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }

  //  public override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
  //      return fetchedResultsController?.sections?[section].name
  //  }

    public override func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        return fetchedResultsController!.sectionForSectionIndexTitle(title, atIndex: index)
    }

    public override func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]! {
        return fetchedResultsController?.sectionIndexTitles
    }

    // MARK: - NSFetchedResultsControllerDelegate

    public func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }

    public func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }

    public func controller(controller: NSFetchedResultsController,
         didChangeSection sectionInfo: NSFetchedResultsSectionInfo,
                 atIndex sectionIndex: Int,
                   forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Automatic)
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Automatic)
        default:
            ()
        }
    }

    public func controller(controller: NSFetchedResultsController,
             didChangeObject anObject: AnyObject,
                atIndexPath indexPath: NSIndexPath,
                   forChangeType type: NSFetchedResultsChangeType,
                         newIndexPath: NSIndexPath) {

        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Automatic)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        case .Update:
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Automatic)
        }
    }
}
