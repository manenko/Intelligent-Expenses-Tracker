//
//  ExpensesByCategoryViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 12.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

public class ExpensesByCategoryViewController: TableViewController, ExpenseDetailsControllerDelegate {
    private var sizingCell: ExpenseCategorySummaryTableViewCell?
    private let cellId = "Cell"

    @IBOutlet
    private weak var btnAddExpense: UIBarButtonItem!

    public func synchronizeWithServer() {
        let client = Client()
        if client.authToken != nil {
            let expenses = Expense.MR_findAll() as [Expense]
            client.postExpenses(expenses) {
                error in
                println(error)
                self.refreshControl?.endRefreshing()
            }
        } else {
            self.refreshControl?.endRefreshing()
        }
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl = UIRefreshControl()
        self.tableView.addSubview(self.refreshControl!)
        self.refreshControl?.addTarget(self, action: "synchronizeWithServer", forControlEvents: .ValueChanged)
    }

    public override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        setupFetchedResultsController()
    }

    public override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return heightForBasicCellAtIndexPath(indexPath)
    }

    public override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = dequeueBrandedCellWithIdentifier(cellId) as ExpenseCategorySummaryTableViewCell
        fillCell(cell, withExpenseCategoryAtIndexPath: indexPath)

        return cell
    }

    public override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier(.ExpensesForCategory, sender: tableView)
    }

    public override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = SegueId(segue: segue)

        switch segueId {
        case .AddExpensesManually:
            // Prepare for this specific segue
            let controller = segue.destinationViewController.childViewControllers!.first as ExpenseDetailsController
            controller.delegate = self
        case .ExpensesForCategory:
            let controller = segue.destinationViewController as ExpensesForCategoryViewController
            let indexPath = tableView.indexPathForSelectedRow()
            let category = fetchedResultsController!.objectAtIndexPath(indexPath!) as ExpenseCategory
            controller.category = category
        case .ScanExpenses:
            ()
        default:
            ()
        }
    }

    func didSaveExpense(controller: ExpenseDetailsController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

    func didCancelExpenseEditing(controller: ExpenseDetailsController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction
    private func addReceiptTapped(sender: UIBarButtonItem) {
        let alertController = createAddExpensesAlertController()

        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender
        }

        presentViewController(alertController, animated: true) {
        }
    }

    private func createAddExpensesAlertController() -> UIAlertController {
        let alertMessage = NSLocalizedString("How you would like to add expenses?", comment: "")
        let alertController = UIAlertController(title: nil, message: alertMessage, preferredStyle: .ActionSheet)

        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) {
            action in
            ()
        }

        let manuallyAction = UIAlertAction(title: NSLocalizedString("Manually", comment: ""), style: .Default) {
            action in
            self.performSegueWithIdentifier(.AddExpensesManually, sender: alertController)
        }

        let scanReceiptAction = UIAlertAction(title: NSLocalizedString("Scan receipt", comment: ""), style: .Default) {
            action in
            self.performSegueWithIdentifier(.ScanExpenses, sender: alertController)
        }

        alertController.addAction(cancelAction)
        alertController.addAction(manuallyAction)
        alertController.addAction(scanReceiptAction)

        alertController.view.tintColor = view.tintColor

        return alertController
    }

    private func fillCell(cell: ExpenseCategorySummaryTableViewCell, withExpenseCategory expenseCategory: ExpenseCategory) {
        let lastUpdate = expenseCategory.lastUpdate == nil ? "" : NSDateFormatter.localizedStringFromDate(expenseCategory.lastUpdate!, dateStyle: .ShortStyle, timeStyle: .ShortStyle)
        cell.title = expenseCategory.name
        cell.details = lastUpdate

        cell.total = expenseCategory.total
        cell.categoryImage = UIImage(named: expenseCategory.id)
    }

    private func fillCell(cell: ExpenseCategorySummaryTableViewCell, withExpenseCategoryAtIndexPath indexPath: NSIndexPath) {
        let expenseCategory = fetchedResultsController!.objectAtIndexPath(indexPath) as ExpenseCategory
        fillCell(cell, withExpenseCategory: expenseCategory)
    }

    private func heightForBasicCellAtIndexPath(indexPath: NSIndexPath) -> CGFloat {
        if sizingCell == nil {
            sizingCell = (tableView.dequeueReusableCellWithIdentifier(cellId) as ExpenseCategorySummaryTableViewCell)
        }

        let cell = sizingCell!
        fillCell(cell, withExpenseCategoryAtIndexPath: indexPath)

        cell.setNeedsLayout()
        cell.layoutIfNeeded()

        let size = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)

        return size.height + 1
    }

    private func setupFetchedResultsController() {
        fetchedResultsController = ExpenseCategory.MR_fetchAllSortedBy("total", ascending: false, withPredicate: nil, groupBy: nil, delegate: self)
        tableView.reloadData()
    }
}
