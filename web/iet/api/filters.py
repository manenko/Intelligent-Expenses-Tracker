import django_filters
from api.models import Expense


class ExpenseFilter(django_filters.FilterSet):
    class Meta:
        model = Expense
        fields = {
            'price': ['lte', 'gte', 'lt', 'gt', 'exact'],
            'date': ['lte', 'gte', 'lt', 'gt', 'exact'],
            'category__id': ['exact'],
            'name': ['exact']
        }
