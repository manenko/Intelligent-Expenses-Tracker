require.config({
    paths: {
        'angular': '../bower_components/angular/angular',
        'angular-route': '../bower_components/angular-route/angular-route',
        'angular-cookies': '../bower_components/angular-cookies/angular-cookies',
        'angular-ipcookie': '../bower_components/angular-cookie/angular-cookie',
        'domReady': '../bower_components/requirejs-domready/domReady',
        'jquery': '../bower_components/jquery/dist/jquery',
        'jquery-ui': '../bower_components/jquery-ui/jquery-ui',
        'highcharts': '../bower_components/highcharts/js/highcharts',
        'highstock': '../bower_components/highstock/js/highstock.src'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular-route': {
            deps: ['angular']
        },
        'angular-cookies': {
            deps: ['angular']
        },
        'angular-ipcookie': {
            deps: ['angular']
        },
        'jquery': {
            exports: 'jQuery'
        },
        'jquery-ui': {
            exports: '$',
            deps: ['jquery']
        },
        'highcharts': {
            exports: 'Highcharts',
            deps: ['jquery']
        },
        'highstock': {
            exports: 'StockChart',
            deps: ['jquery']
        }
    },
    deps: ['app/bootstrap.js']
});