//
//  Client.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 11.03.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import Foundation
import Alamofire
import KeychainAccess

class Client {

    private let keychain: Keychain
    private let tokenKey = "token"

    var authToken: String? {
        get {
            return self.keychain[self.tokenKey]
        }

        set {
            self.keychain[self.tokenKey] = newValue
        }
    }

    private struct Auth {
        private static let tokenService = "com.manenko.iet-token"
        private static let tokenKey = "token"
    }

    private struct Endpoints {
        static let root = "https://fast-gorge-6167.herokuapp.com/"
        //static let root = "http://10.0.1.5:8000/"
        static let apiRoot = root + "api/"
        static let expenses = apiRoot + "expenses/"

        static let tokenByUserNameAndPassword = root + "api-token-auth/"
    }

    init(authToken: String?) {
        self.keychain = Keychain(service: Auth.tokenService)
            .label("IET API access token")
            .synchronizable(true)
        self.authToken = authToken
    }

    init() {
        self.keychain = Keychain(service: Auth.tokenService)
            .label("IET API access token")
            .synchronizable(true)
    }

    func authenticate(username: String, password: String, callback: (error: NSError?) -> ()) {
        let data = ["username": username, "password": password]
        Alamofire.request(.POST, Endpoints.tokenByUserNameAndPassword, parameters: data, encoding: .JSON).responseSwiftyJSON {
            (request, response, json, error) in

            if error == nil {
                let statusCode = response!.statusCode

                if statusCode == 200 {
                    self.authToken = json["token"].object as? String
                    callback(error: nil)
                } else {
                    let userInfo = [NSLocalizedDescriptionKey: "Unable to sign in with provided credentials."]
                    callback(error: NSError(domain: "com.manenko.iet", code: statusCode, userInfo: userInfo))
                }
            } else {
                callback(error: error)
            }
        }
    }

    func postExpenses(expenses: [Expense], callback: (error: NSError?) -> ()) {
        if let token = self.authToken {

            self.deleteAllExpenses() {
                error in

                if error == nil {
                    let timeOffset = Int64(CDouble(2) * CDouble(NSEC_PER_SEC))
                    let time = dispatch_time(DISPATCH_TIME_NOW, timeOffset)
                    dispatch_after(time, dispatch_get_main_queue()) {
                        let request = NSMutableURLRequest(URL: NSURL(string: Endpoints.expenses)!)
                        request.HTTPMethod = "POST"
                        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                        request.setValue("Token " + token, forHTTPHeaderField: "Authorization")

                        let json = self.getAllExpenses(expenses)
                        var error: NSError?
                        let data = NSJSONSerialization.dataWithJSONObject(
                            json,
                            options: .allZeros,
                            error: &error)
                        request.HTTPBody = data

//                        let jsonString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                        println(jsonString)

                        Alamofire.request(request).responseSwiftyJSON {
                            (request, response, json, error) in

                            if error == nil {
                                println(response?.statusCode)
                                println(json)
                            }
                            
                            callback(error: error)
                        }
                    }
                } else {
                    // Failed to remove expenses
                    callback(error: error)
                }
            }
        } else {
            assert(false, "Please authenticate first!")
        }
    }

    func deleteAllExpenses(callback: (error: NSError?) -> ()) {
        if let token = self.authToken {
            let request = NSMutableURLRequest(URL: NSURL(string: Endpoints.expenses)!)
            request.HTTPMethod = "DELETE"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Token " + token, forHTTPHeaderField: "Authorization")

            var error: NSError?
            request.HTTPBody = nil
            Alamofire.request(request).responseSwiftyJSON {
                (request, response, json, error) in
                callback(error: error)
            }
        } else {
            assert(false, "Please authenticate first!")
        }
    }

    private func getAllExpenses(expenses: [Expense]) -> [[String: AnyObject]] {
        var exp = expenses.map { [
            "name": $0.name as AnyObject,
            "price": $0.price,
            "date": NSDate.ISOStringFromDate($0.date),
            "category": $0.category.id
            ]
        }

        return exp
    }
}

