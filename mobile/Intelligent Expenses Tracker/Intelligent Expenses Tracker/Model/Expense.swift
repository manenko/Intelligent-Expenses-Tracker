//
//  Expense.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 20.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import Foundation

@objc(Expense)
class Expense: _Expense {
    var localizedPrice: String {
        get {
            return NSNumberFormatter.localizedStringFromNumber(price, numberStyle: .CurrencyStyle)
        }

        set {
            price = NSDecimalNumber(string: newValue, locale: NSLocale.currentLocale())
        }
    }
}
