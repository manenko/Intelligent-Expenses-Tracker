//
//  AppDelegate.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 06.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit
import CoreData

private let StoreName = "IET.sqlite"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        MagicalRecord.setupCoreDataStackWithAutoMigratingSqliteStoreNamed(StoreName)
        
        Localizer(managedObjectContext: NSManagedObjectContext.MR_defaultContext()).localize()

        setupTabBar()

        return true
    }

    func applicationWillTerminate(application: UIApplication) {
        MagicalRecord.cleanUp()
    }

    private func setupTabBar() {
        // Workaround. I receive errors like:
        // >>> 2014-12-06 18:59:43.277 Intelligent Expenses Tracker[50597:2175318] CUICatalog: Invalid asset name supplied: (null)
        // This is because I set up Tab Bar Item's selected image using IB.
        // However, it works, if I set it programmatically.
        if let controller = window?.rootViewController as? UITabBarController {
            (controller.tabBar.items![0] as UITabBarItem).selectedImage = UIImage(named: "Expenses-Selected")
            (controller.tabBar.items![1] as UITabBarItem).selectedImage = UIImage(named: "Reports-Selected")
            (controller.tabBar.items![2] as UITabBarItem).selectedImage = UIImage(named: "Settings-Selected")

            // Now, let's change tint color of the tab bar
            controller.tabBar.tintColor = UIColor.orangeColor()
        }
    }
}

