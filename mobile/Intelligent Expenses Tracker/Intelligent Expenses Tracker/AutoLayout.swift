//
//  AutoLayout.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 16.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit

private func calculateLabelWidth(label: UILabel) -> CGFloat {
    let labelSize = label.sizeThatFits(CGSize(width: CGFloat.max, height: label.frame.height))

    return labelSize.width
}

private func calculateMaxLabelWidth(labels: [UILabel]) -> CGFloat {
    return reduce(map(labels, calculateLabelWidth), 0, max)
}

public func makeLabelsToHaveSameWidth(labels: [UILabel]) {
    let maxLabelWidth = calculateMaxLabelWidth(labels)
    for label in labels {
        let constraint = NSLayoutConstraint(item: label,
            attribute: .Width,
            relatedBy: .Equal,
            toItem: nil,
            attribute: .NotAnAttribute,
            multiplier: 1,
            constant: maxLabelWidth)
        //constraint.priority = 999
        label.addConstraint(constraint)
    }
}