// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Receipt.swift instead.

import CoreData

enum ReceiptAttributes: String {
    case imageData = "imageData"
}

enum ReceiptRelationships: String {
    case expenses = "expenses"
}

@objc
class _Receipt: NSManagedObject {

    // MARK: - Class methods

    class func entityName () -> String {
        return "Receipt"
    }

    class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }

    // MARK: - Life cycle methods

    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

    convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = _Receipt.entity(managedObjectContext)
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged
    var imageData: NSData

    // func validateImageData(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    // MARK: - Relationships

    @NSManaged
    var expenses: NSSet

}

extension _Receipt {

    func addExpenses(objects: NSSet) {
        let mutable = self.expenses.mutableCopy() as NSMutableSet
        mutable.unionSet(objects)
        self.expenses = mutable.copy() as NSSet
    }

    func removeExpenses(objects: NSSet) {
        let mutable = self.expenses.mutableCopy() as NSMutableSet
        mutable.minusSet(objects)
        self.expenses = mutable.copy() as NSSet
    }

    func addExpensesObject(value: Expense!) {
        let mutable = self.expenses.mutableCopy() as NSMutableSet
        mutable.addObject(value)
        self.expenses = mutable.copy() as NSSet
    }

    func removeExpensesObject(value: Expense!) {
        let mutable = self.expenses.mutableCopy() as NSMutableSet
        mutable.removeObject(value)
        self.expenses = mutable.copy() as NSSet
    }

}
