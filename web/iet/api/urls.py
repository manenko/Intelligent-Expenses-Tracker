from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from api import views


urlpatterns = [
    url(r'^expenses/$', views.ExpenseList.as_view()),
    url(r'^expenses/(?P<pk>[0-9]+)/$', views.ExpenseDetail.as_view()),
    url(r'^expense_categories/$', views.ExpenseCategoryList.as_view()),
    url(r'^expense_categories/(?P<pk>.+)/$', views.ExpenseCategoryDetail.as_view()),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
