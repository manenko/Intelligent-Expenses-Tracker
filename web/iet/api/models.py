from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class ExpenseCategory(models.Model):
    id = models.CharField(max_length=100, unique=True, primary_key=True)
    name = models.CharField(max_length=100)
    summary = models.TextField(blank=True, default='')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()


class Expense(models.Model):
    owner = models.ForeignKey('auth.User', related_name='expenses')
    category = models.ForeignKey(ExpenseCategory)

    date = models.DateTimeField()
    name = models.CharField(max_length=200)
    # TODO: Consider to change 'price' to 'amount'
    price = models.DecimalField(decimal_places=2, max_digits=15)

    def __unicode__(self):
        return '{0} {1}'.format(self.name, self.price)

    def __str__(self):
        return self.__unicode__()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
