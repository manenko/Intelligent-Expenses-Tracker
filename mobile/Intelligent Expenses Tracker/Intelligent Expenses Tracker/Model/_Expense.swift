// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Expense.swift instead.

import CoreData

enum ExpenseAttributes: String {
    case date = "date"
    case name = "name"
    case price = "price"
}

enum ExpenseRelationships: String {
    case category = "category"
    case receipt = "receipt"
}

@objc
class _Expense: NSManagedObject {

    // MARK: - Class methods

    class func entityName () -> String {
        return "Expense"
    }

    class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }

    // MARK: - Life cycle methods

    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

    convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = _Expense.entity(managedObjectContext)
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged
    var date: NSDate

    // func validateDate(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var name: String

    // func validateName(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var price: NSDecimalNumber

    // func validatePrice(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    // MARK: - Relationships

    @NSManaged
    var category: ExpenseCategory

    // func validateCategory(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

    @NSManaged
    var receipt: Receipt?

    // func validateReceipt(value: AutoreleasingUnsafePointer<AnyObject>, error: NSErrorPointer) {}

}

