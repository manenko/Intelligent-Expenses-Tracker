/**
 * Created by Home-PC on 28.02.2015.
 */
define([
    './module',
    'jquery'
], function(services, $){
    services.factory('mainExChart', [function(){
        var exp = {},
            chartZone = $('#oa-main-ex-charts'),
            charts = {
                'bar': drawBarChart,
                'pie': drawPie,
                'spline': drawSpline
            };

        function hideLaunch(){
            chartZone.children().hide();
        }

        function dropCharts(){
            chartZone.children('div').remove();
        }

        function r1Details(info){
            $('#oa-main-ex-r1-details-container') ? $('#oa-main-ex-r1-details-container').remove() : 'null';

            var details = document.createElement('div');

            details.className = '';
            chartZone.append(details);

            chartZone.append('<div class="oa-main-ex-chart-r1-details" id="oa-main-ex-r1-details-container">' +
                '<table class="oq-main-ex-r1-tab">' +
                    '<thead>' +
                        '<tr>' +
            '<td>Об\'єкт витрати</td>' +
            '<td>Витрачено коштів (долл.)</td>' +
                        '</tr>' +
                    '</thead>' +
            '<tbody id="oa-main-ex-r1-details-body"></tbody>' +
                '</table>' +
            '</div>');

            var detailBody = $('#oa-main-ex-r1-details-body');

            info.forEach(function(item){
                detailBody.append('<tr>' +
                '<td>' + item.name + '</td>' +
                '<td>' + item.price.toFixed(2) + '</td>' +
                '</tr>');
            });
        }

        exp.drawChartR1 = function(drawTo){
            var container = prepareChartZone(drawTo),
                options = {
                    title: 'Загалні витрати',
                    navigator: true,
                    rangeSelector: true,
                    clickable: true,
                    url: '/expenses/details?day={params}'
                };

            $.ajax({
                url: '/expenses/all',
                method: 'GET'
            }).then(function(response){
                drawSpline(container, response, options);
            }, function(){

            });
        };

        exp.drawChartR2 = function(drawTo){
            var container = prepareChartZone(drawTo),
                options = {
                    title: 'Ваші витрати по роках',
                    details: [
                        {
                            type: 'pie',
                            url: '/expenses/categories/?year={param}',
                            clickable: false,
                            title: 'Ваші втирати за {param} рік по категоріям'
                        },
                        {
                            type: 'bar',
                            url: '/expenses/details?year={param}&aggregate=month',
                            clickable: false,
                            title: 'Ваші втирати за {param} рік по місяцям'
                        }
                    ],
                    clickable: true
                };

            $.ajax({
                url: '/expenses/years/'
            }).then(function(response){
                drawBarChart(container, response, options);
            });
        };

        exp.drawChartR3 = function(drawTo){
            var container = prepareChartZone(drawTo),
                options = {
                    title: 'Ваші витрати по місяцях',
                    details: [
                        {
                            type: 'pie',
                            url: '/expenses/categories/?month={param}',
                            clickable: false,
                            title: 'Ваші втирати за {param} місяць по категоріям'
                        },
                        {
                            type: 'spline',
                            url: '/expenses/details?month={param}&aggregate=day',
                            clickable: true,
                            clickUrl: '/expenses/details?day={params}',
                            title: 'Ваші витрати за {param} місяць по дням'
                        }
                    ],
                    clickable: true
                };

            $.ajax({
                url: '/expenses/months/'
            }).then(function(response){
                drawBarChart(container, response, options);
            });
        };

        exp.drawChartR4 = function(drawTo){
            var container = prepareChartZone(drawTo),
                options = {
                    title: 'Ваші витрати по тижням',
                    details: [
                        {
                            type: 'spline',
                            url: '/expenses/details?week={param}&aggregate=day',
                            clickable: true,
                            clickUrl: '/expenses/details?day={params}',
                            title: 'Ваші витрати за {param} тиждень по дням'
                        }
                    ],
                    clickable: true,
                    labels: {
                        rotation: -90,
                        padding: 20
                    }
                };

            $.ajax({
                url: '/expenses/weeks/'
            }).then(function(response){
                drawBarChart(container, response, options);
            });
        };

        exp.drawChartR5 = function(drawTo){
            var container = prepareChartZone(drawTo),
                options = {
                    title: 'Ваші витрати по категоріям',
                    clickable: true,
                    details: [
                        {
                            type: 'spline',
                            title: 'Динаміка витрат по категорії {param}',
                            url: '/expenses/details?category={param}&aggregate=day',
                            clickable: true,
                            clickUrl: '/expenses/details?category='
                        }
                    ]
                };

            $.ajax({
                url: '/expenses/categories/'
            }).then(function(response){
                drawPie(container, response, options);
            });
        };

        exp.drawChartR6 = function(drawTo){
            var container = document.createElement('div');

            hideLaunch();
            dropCharts();

            container.className = 'oa-chart-container';
            drawTo.append(container);

            $.ajax({
                url: '/getTopList',
                method: 'GET'
            }).then(function(response){
                var acc = document.createElement('div');

                acc.id = 'accordeon';

                $(container).append(acc);

                acc = $(acc);

                for(var n in response){
                    if(response.hasOwnProperty(n)){
                        acc.append('<h3>' + response[n].name + '</h3>');

                        var dataDIV = document.createElement('div');

                        acc.append(dataDIV);

                        var table = document.createElement('table');

                        table.className = 'oa-main-ex-top-table';

                        $(dataDIV).append(table);

                        $(table).append('<thead>' +
                        '<tr>' +
                        '<td>#</td>' +
                        '<td>Назва товару</td>' +
                        '<td>Дата покупки</td>' +
                        '<td>Сума в долл.</td>' +
                        '</tr>' +
                        '</thead>');

                        var tbody = document.createElement('tbody');

                        $(table).append(tbody);

                        var counter = 1;

                        response[n].data.forEach(function(item){
                            $(tbody).append('<tr>' +
                            '<td>' + counter + '</td>' +
                            '<td>' + item.name + '</td>' +
                            '<td>' + item.date + '</td>' +
                            '<td>' + item.price.toFixed(2) + '</td>' +
                            '</tr>');

                            counter++;
                        });
                    }
                }

                acc.accordion();
            });
        };

        function prepareChartZone(drawTo){
            var container = document.createElement('div');

            hideLaunch();
            dropCharts();

            container.className = 'oa-chart-container';
            drawTo.append(container);

            return container;
        }

        function drawBarChart(to, data, options){
            var dataChart = {},
                click = null;

            if(options.clickable){
                click = function(){
                    var param = this.category;

                    $('.oa-chart-container-r2-details').remove();

                    options.details.forEach(function(item){
                        var c = document.createElement('div'),
                            thisOptions = {
                                clickable: item.clickable,
                                url: item.clickUrl
                            },
                            url;

                        c.className = 'oa-chart-container-r2-details';
                        chartZone.append(c);

                        url = item.url.replace('{param}', param);

                        $.ajax({
                            url: url
                        }).then(function(response){
                            thisOptions.title = item.title.replace('{param}', param);
                            charts[item.type](c, response, thisOptions);
                        });
                    });
                };
            }

            dataChart.categories = [];
            dataChart.series = [];

            for(var n in data){
                if(data.hasOwnProperty(n)){
                    dataChart.categories.push(n);
                    dataChart.series.push(data[n]);
                }
            }

            $(to).highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: options.title
                },
                xAxis: {
                    categories: dataChart.categories,
                    labels: {
                        rotation: options.labels ? options.labels.rotation : 0,
                        padding: options.labels ? options.labels.padding : 0,
                        enabled: dataChart.series.length < 25 ? true : false
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Сума в долл.'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">Рік: {point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}$</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0,
                        dataLabels:{
                            enabled: true,
                            style:{
                                fontWeight: 'bold'
                            },
                            format: '{y:.0f}$'
                        },
                        color: '#FF6D00'
                    },
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: click
                            }
                        }
                    }
                },
                series: [{
                    name: 'Рівень витрат',
                    data: dataChart.series
                }]
            });
        }

        function drawSpline(to, data, options){
            var arr = [],
                click = null;

            if(options.clickable){
                click = function(){
                    var date = (new Date(this.x)).toISOString().substring(0, 10),
                        url = options.url || options.clickUrl;

                    $.ajax({
                        url: url.replace('{params}', date)
                    }).then(function(response){
                        r1Details(response.items);
                    });
                }
            }

            for(var n in data){
                if(data.hasOwnProperty(n)){
                    arr.push([
                        +(new Date(n)),
                        data[n]
                    ]);
                }
            }

            $(to).highcharts({
                chart: {
                    type: 'spline'
                },
                title: {
                    text: options.title
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Сума витрат (долл.)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">Рік: {point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}$</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                navigator: {
                    enabled: options.navigator
                },
                rangeSelector: {
                    enabled: options.rangeSelector,
                    allButtonsEnabled: true,
                    inputEnabled: false
                },
                plotOptions:{
                    spline:{
                        marker: {
                            enabled: false
                        },
                        lineWidth: 1
                    },
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: click
                            }
                        }
                    }
                },
                series: [{
                    name: 'Динаміка витрат',
                    data: arr
                }]
            });
        }

        function drawPie(to, data, options){
            var dataChart = [],
                click = null;

            if(options.clickable){
                click = function(){
                    var catId = this.name;

                    options.details.forEach(function(item){
                        var c = document.createElement('div'),
                            thisOptions = {
                                clickable: item.clickable,
                                url: '/expenses/details?category=' + catId + '&day={params}'
                            },
                            url;

                        c.className = 'oa-chart-container-r2-details';
                        chartZone.append(c);

                        url = item.url.replace('{param}', catId);

                        $.ajax({
                            url: url
                        }).then(function(response){
                            thisOptions.title = item.title.replace('{param}', catId);
                            charts[item.type](c, response, thisOptions);
                        });
                    });
                }
            }

            for(var n in data){
                if(data.hasOwnProperty(n)){
                    dataChart.push([
                        n,
                        data[n]
                    ]);
                }
            }

            $(to).highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: options.title
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    },
                    series: {
                        point: {
                            events: {
                                click: click
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    data: dataChart
                }]
            });
        }

        return exp;
    }
])});