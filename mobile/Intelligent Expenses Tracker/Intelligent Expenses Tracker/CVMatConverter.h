//
//  CVMatConverter.h
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 07.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#include <opencv2/opencv.hpp>

@interface CVMatConverter : NSObject

+ (cv::Mat)cvMatFromUIImage:(UIImage *)image;

+ (UIImage *)imageFromCvMat:(const cv::Mat&)mat;

+ (void)convertYUVSampleBuffer:(CMSampleBufferRef)buf toGrayscaleMat:(cv::Mat &)mat;

@end
