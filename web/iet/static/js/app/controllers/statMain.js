/**
 * Created by Home-PC on 15.02.2015.
 */

define([
    './module',
    'jquery',
    'jquery-ui',
    'highstock'
], function(controllers, $){
    controllers.controller('statMain', ['mainExChart', 'DataModel', '$scope', 'ipCookie', function(mainExChart, DataModel, $scope, ipCookie){
        var contentZone = $('#oa-main-ex-contents'),
            reports = {
                r1: {
                    build: mainExChart.drawChartR1
                },
                r2: {
                    build: mainExChart.drawChartR2
                },
                r3: {
                    build: mainExChart.drawChartR3
                },
                r4: {
                    build: mainExChart.drawChartR4
                },
                r5: {
                    build: mainExChart.drawChartR5
                },
                r6: {
                    build: mainExChart.drawChartR6
                },
                r7: {
                    build: mainExChart.drawChartR7
                }
            };

        window.activeChart = null;

        $( "#tabs" ).tabs();

        var dm = new DataModel('/api/expenses/');

        contentZone.click(function(me){
            var target = me.target,
                main = target.tagName == 'LI' ? target : target.parentNode,
                reportName = main.getAttribute('data-report'),
                updateBtn = $('#oa-update-btn');

            var actives = $('.oa-ex-contents-item-active');

            if(actives){
                actives.removeClass('oa-ex-contents-item-active');
            }

            $(main).addClass('oa-ex-contents-item-active');

            updateBtn.attr('disabled', false);

            updateBtn.click(function(){
                dm = new DataModel('/api/expenses/');

                var timer = setInterval(function(){
                    if(dm.data){
                        reports[window.activeChart].build(dm);
                        clearInterval(timer);
                    }
                }, 0);
            });

            window.activeChart = reportName;

            reports[reportName].build(dm);
        });
    }]);
});