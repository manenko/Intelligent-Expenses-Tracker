from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework_bulk import BulkSerializerMixin, BulkListSerializer
from api.models import Expense, ExpenseCategory


class ExpenseCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpenseCategory


class ExpenseSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Expense
        list_serializer_class = BulkListSerializer


class UserSerializer(serializers.ModelSerializer):
    expenses = serializers.PrimaryKeyRelatedField(many=True, queryset=Expense.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'expenses', 'first_name', 'last_name')
