//
//  HelpViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 20.03.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    @IBAction func didTapInitializeWithTestData(sender: UIButton) {
        let context = NSManagedObjectContext.MR_context()
        initializeWithTestData(context)
    }
    @IBAction func didTapDeleteAllExpenses(sender: UIButton) {
        let context = NSManagedObjectContext.MR_context()
        Expense.MR_truncateAllInContext(context)
        let categories = ExpenseCategory.MR_findAllInContext(context) as [ExpenseCategory]
        for category in categories {
            category.total = NSDecimalNumber.zero()
        }

        context.MR_saveToPersistentStoreAndWait()
    }
}
