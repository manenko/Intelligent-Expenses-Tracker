/**
 * Created by Home-PC on 15.03.2015.
 */

(function(){
    Date.prototype.Week = function(){
        var that = this,
            inner = {};

        inner.getNumber = function(){
            return +inner.week
        };

        inner.getString = function(){
            return inner.week + '-' + (new Date(+that)).getFullYear();
        };

        inner.getWeekStart = function(){
            return new Date(that - 1000 * 60 * 60 * 24 * (inner.weekDay - 1));
        };

        inner.getWeekEnd = function(){
            return new Date(+that + 1000 * 60 * 60 * 24 * (7 - inner.weekDay));
        };

        function load(){
            var date = new Date(+that);

            date.setHours(0, 0, 0);
            date.setDate(date.getDate() + 4 - (date.getDay() || 7));

            var week = Math.ceil((((date - new Date(date.getFullYear(),0,1))/8.64e7)+1)/7);

            inner.week = week < 10 ? '0' + week : week;
            inner.weekDay = ((that.getDay() + 6) % 7) + 1;
        }

        load();

        return inner;
    }

    Date.prototype.Year = function(){
        var that = this,
            inner = {};

        inner.getYear = function(){
            return inner.year;
        };

        inner.getYearStart = function(){
            return new Date(inner.year, 0, 1);
        };

        inner.getYearEnd = function(){
            return new Date(inner.year, 11, 31);
        };

        function load(){
            inner.year = (new Date(+that)).getFullYear();
        }

        load();

        return inner;
    };

    Date.prototype.Month = function(){
        var that = this,
            inner = {};

        inner.getString = function(){
            return (inner.month < 10 ? '0' + inner.month : inner.month) + '-' + (new Date(+that)).getFullYear();
        };

        inner.getMonthStart = function(){
            var date = new Date(+that);
            return new Date(date.getFullYear(), inner.month - 1, 1);
        };

        inner.getMonthEnd = function(){
            var date = new Date(+that);
            return new Date(+(new Date(date.getFullYear(), inner.month, 1)) - 1000 * 60 * 60 * 24);
        };

        function load(){
            inner.month = (new Date(+that)).getMonth() + 1;
        }

        load();

        return inner;
    };

    String.prototype.stringMonthToInt = function(){
        var year = +this.substring(3),
            month = this.substring(0, 2) - 1;

        console.log(month + '-' + year);

        return +(new Date(year, month, 1));
    };

})()