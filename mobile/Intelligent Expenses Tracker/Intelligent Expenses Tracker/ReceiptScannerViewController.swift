//
//  ReceiptScannerViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 26.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation

class ReceiptScannerViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    private var captureSession: AVCaptureSession!
    private var videoDataOutput: AVCaptureVideoDataOutput!
    private var originalImage: UIImage?
    private var backCameraDevice: AVCaptureDevice?

    @IBAction func didTouchScanButton(sender: UIButton) {
        let CameraShutterSound: SystemSoundID = 1108
        AudioServicesPlaySystemSound(CameraShutterSound)
        stopRunning()
    }

    @IBAction func didTouchCancelButton(sender: UIButton) {
        dispatch_after(0, dispatch_get_main_queue()) {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }

    @IBOutlet var imageView: UIImageView!

    func renderFrame(frame: UIImage!) {
        originalImage = frame
        imageView.image = frame//CVImagePreprocessor.prepareImageForLiveView(frame)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if let camera = backCameraDevice {
            setupCameraDevice(camera)
        }

        startRunning()

    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        stopRunning()
    }

    override func viewDidLoad() {
        setupCameraSession()
        updateVideoOrientation()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = SegueId(segue: segue)

        if segueId == .RecognizeReceiptImage {
            let controller = segue.destinationViewController as? ReceiptRecognizerViewController
            controller?.receiptImage = originalImage
        }
    }

    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        updateVideoOrientation()
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }

    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        let image = ImageConverter.imageFromSampleBuffer(sampleBuffer)

        dispatch_sync(dispatch_get_main_queue()) {
            self.renderFrame(image)
        }
    }

    private func setupCameraSession() {
        if captureSession == nil {
            captureSession = AVCaptureSession()
            captureSession.sessionPreset = AVCaptureSessionPresetHigh

            if let backCamera = findBackCameraDevice() {
                setupCameraDevice(backCamera)

                backCameraDevice = backCamera

                var error: NSError?
                let backCameraInput = AVCaptureDeviceInput.deviceInputWithDevice(backCamera, error: &error) as? AVCaptureDeviceInput
                if error == nil {
                    if captureSession.canAddInput(backCameraInput) {
                        captureSession.addInput(backCameraInput)
                        videoDataOutput = AVCaptureVideoDataOutput()
                        videoDataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey: kCVPixelFormatType_32BGRA]
                        videoDataOutput.alwaysDiscardsLateVideoFrames = true

                        if captureSession.canAddOutput(videoDataOutput) {
                            captureSession.addOutput(videoDataOutput)
                            captureSession.commitConfiguration()

                            let queue = dispatch_queue_create("ScannerViewControllerVideoQueue", DISPATCH_QUEUE_SERIAL)
                            videoDataOutput.setSampleBufferDelegate(self, queue: queue)
                        } else {
                            println("Can not add device output to the AV session.")
                        }
                    } else {
                        println("Can not add back camera as a device input to the AV session.")
                    }
                } else {
                    println(error?.localizedDescription)
                }
            } else {
                println("Can't find back camera device")
            }
        }
    }

    private func setupCameraDevice(cameraDevice: AVCaptureDevice) {
        if cameraDevice.hasTorch && cameraDevice.hasFlash {
            var error: NSError?
            if cameraDevice.lockForConfiguration(&error) {
                cameraDevice.torchMode = .On
                cameraDevice.flashMode = .On
                cameraDevice.setTorchModeOnWithLevel(AVCaptureMaxAvailableTorchLevel, error: &error)
                cameraDevice.unlockForConfiguration()
            } else {
                println(error)
            }
        }
    }

    private func findBackCameraDevice() -> AVCaptureDevice? {
        let videoCaptureDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)

        return find(videoCaptureDevices, {$0.position == AVCaptureDevicePosition.Back}) as AVCaptureDevice?
    }

    private func startRunning() {
        captureSession?.startRunning()
    }

    private func stopRunning() {
        captureSession?.stopRunning()
    }

    /// Set video orientation based on the device orientation to ensure the pic is right side up for all orientations.
    private func updateVideoOrientation() {
        var videoOrientation: AVCaptureVideoOrientation

        switch UIDevice.currentDevice().orientation {
        case .LandscapeLeft:
            // Not clear why but the landscape orientations are reversed.
            // If I use AVCaptureVideoOrientationLandscapeLeft here the pic ends up upside down.
            videoOrientation = .LandscapeRight
        case .LandscapeRight:
            // Not clear why but the landscape orientations are reversed.
            // If I use AVCaptureVideoOrientationLandscapeRight here the pic ends up upside down.
            videoOrientation = .LandscapeLeft
        case .PortraitUpsideDown:
            videoOrientation = .PortraitUpsideDown
        default:
            videoOrientation = .Portrait
        }

        let captureConnection = videoDataOutput.connections.first as? AVCaptureConnection
        captureConnection?.videoOrientation = videoOrientation
    }
}
