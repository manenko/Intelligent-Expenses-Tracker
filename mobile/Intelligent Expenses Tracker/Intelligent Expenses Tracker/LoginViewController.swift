//
//  LoginViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 13.03.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var delegate: LoginViewControllerDelegate?

    @IBAction func didTapCancel(sender: UIButton) {
        self.delegate?.didCancelSignIn(self)
    }

    @IBAction func fieldEditingChanged(sender: UITextField) {
        self.updateSignInButtonState()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.hidden = true

        self.updateSignInButtonState()

        self.userNameField.delegate = self
        self.passwordField.delegate = self
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.userNameField {
            return self.passwordField.becomeFirstResponder()
        } else {
            if self.canSignIn {
                textField.endEditing(true)
                self.signIn()
            }
        }

        return true
    }

    func updateSignInButtonState() {
        self.signInButton.enabled = self.canSignIn
        self.signInButton.backgroundColor = self.canSignIn ? UIColor.orangeColor() : UIColor.lightGrayColor()
    }

    private var canSignIn: Bool {
        get {
            return !self.passwordField.text.isEmpty && !self.userNameField.text.isEmpty
        }
    }

    @IBAction func didTapInsideView(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

    @IBAction func didTapSignIn(sender: UIButton) {
        self.signIn()
    }

    @IBAction func didTapSignUp(sender: UIButton) {
        let str = "http://fast-gorge-6167.herokuapp.com/accounts/register/"
        if let registrationPageUrl = NSURL(string: str) {
            UIApplication.sharedApplication().openURL(registrationPageUrl)
        } else {
            assert(false, "Registration Page URL is invalid")
        }
    }

    private func signIn() {
        if self.canSignIn {
            let username = self.userNameField.text
            let password = self.passwordField.text

            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            self.signInButton.enabled = false
            self.signInButton.backgroundColor = UIColor.lightGrayColor()
            self.userNameField.enabled = false
            self.passwordField.enabled = false

            let client = Client()
            client.authenticate(username, password: password) {
                error in
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidden = true
                self.signInButton.enabled = true
                self.userNameField.enabled = true
                self.passwordField.enabled = true
                self.signInButton.backgroundColor = UIColor.orangeColor()

                if let authError = error {
                    let alertController = UIAlertController(
                        title: NSLocalizedString("Error", comment: "Error title for alert dialog"),
                        message: authError.localizedDescription,
                        preferredStyle: .Alert)

                    let cancelAction = UIAlertAction(
                        title: NSLocalizedString("OK", comment: "OK button text"),
                        style: .Cancel, handler: nil)

                    alertController.addAction(cancelAction)
                    self.presentViewController(alertController, animated: true) {
                    }
                } else {
                    self.delegate?.didSignIn(self)
                }
            }
        }
    }
}
