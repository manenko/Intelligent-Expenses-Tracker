//
//  Functional.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 04.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import Foundation

public func find<S: SequenceType>(source: S, predicate: (S.Generator.Element) -> Bool) -> S.Generator.Element? {
    var element: S.Generator.Element? = nil

    for e in source {
        if predicate(e) {
            element = e
            break
        }
    }

    return element
}
