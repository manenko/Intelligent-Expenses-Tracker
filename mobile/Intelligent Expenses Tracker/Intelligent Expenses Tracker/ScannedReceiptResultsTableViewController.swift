//
//  ScannedReceiptResultsViewController.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 25.01.15.
//  Copyright (c) 2015 Oleksandr Manenko. All rights reserved.
//

import UIKit

class ScannedReceiptResultsTableViewController: BrandedTableViewController, ExpenseDetailsControllerDelegate {
    var scanResults: [ExpenseElement]?

    private var expenses = [Expense]()

    func save() {
        objectContext.MR_saveToPersistentStoreAndWait()
    }

    private var objectContext: NSManagedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadExpenses()
    }

    func loadExpenses() {
        expenses = [Expense]()

        if let results = scanResults {
            objectContext = NSManagedObjectContext.MR_context()

            let categoryIds = ReceiptRecognizerViewController.testCategories()

            for i in 0..<categoryIds.count {
                let expenseElement = results[i]
                let expense = Expense(managedObjectContext: objectContext)
                expense.date = NSDate()
                expense.name = expenseElement.name
                expense.price = expenseElement.price.totalPrice

                let category = ExpenseCategory.MR_findFirstByAttribute("id", withValue: categoryIds[i], inContext: objectContext) as ExpenseCategory
                category.total = expense.price.decimalNumberByAdding(category.total)
                category.lastUpdate = expense.date
                category.addExpensesObject(expense)

                expenses.append(expense)
            }

//            for expenseElement in results {
//                let expense = Expense(managedObjectContext: objectContext)
//                expense.date = NSDate()
//                expense.name = expenseElement.name
//                expense.price = expenseElement.price.totalPrice
//
//                let category = ExpenseCategory.defaultCategoryInContext(objectContext)
//                category.total = expense.price.decimalNumberByAdding(category.total)
//                category.lastUpdate = expense.date
//                category.addExpensesObject(expense)
//
//                expenses.append(expense)
//            }

//            tableView.reloadData()
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenses.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellId = "Expense Cell"
        let cell = dequeueBrandedCellWithIdentifier(cellId) as ExpenseCell
        let expense = expenses[indexPath.row]
        cell.expense = expense

        return cell
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = SegueId(segue: segue)

        switch segueId {
        case .EditScannedExpense:
            let destinationController = segue.destinationViewController.childViewControllers!.first as ExpenseDetailsController
            let index = tableView.indexPathForSelectedRow()
            let expense = expenses[index!.item]
            destinationController.expense = expense
            destinationController.delegate = self
            destinationController.shouldSave = false
        default:
            ()
        }
    }

    func didCancelExpenseEditing(controller: ExpenseDetailsController) {
        controller.dismissViewControllerAnimated(true) {
            self.tableView.reloadData()
        }
    }

    func didSaveExpense(controller: ExpenseDetailsController) {
        controller.dismissViewControllerAnimated(true) {
            self.tableView.reloadData()
        }
    }
}
