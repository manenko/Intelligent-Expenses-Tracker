//
//  SegueId.swift
//  Intelligent Expenses Tracker
//
//  Created by Oleksandr Manenko on 14.12.14.
//  Copyright (c) 2014 Oleksandr Manenko. All rights reserved.
//

import UIKit

public enum SegueId: String {
    case Unknown = "Cuy'val Dar"

    case AddExpensesManually = "Add expenses manually"
    case SelectExpenseCategory = "Select Expense Category Segue"
    case ExpensesForCategory = "Expenses for Category"
    case ScanExpenses = "Scan Expenses"
    case RecognizeReceiptImage = "Recognize Receipt Image"
    case ReceiptRecognizerProgress = "Receipt Recognizer Progress"
    case ShowScannedReceiptResults = "Show Scanned Receipt Results"
    case ShowScannedReceiptResultsTable = "Show Scanned Receipt Results Table"
    case EditExistingExpense = "Edit Existing Expense"
    case EditScannedExpense = "Edit Scanned Expense"
    case Login = "Login Page"
    case About = "About"
    case Help = "Help"

    public init(segue: UIStoryboardSegue) {
        self = SegueId(rawValue: segue.identifier ?? SegueId.Unknown.rawValue) ?? SegueId.Unknown
        assert(self != .Unknown)
    }
}
