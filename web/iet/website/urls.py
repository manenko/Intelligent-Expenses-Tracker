from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from rest_framework.authtoken import views
from website.decorators import anonymous_required

urlpatterns = patterns('',
                       url(r'^$', anonymous_required(TemplateView.as_view(template_name='index.html'))),
                       url(r'^main/$', login_required(TemplateView.as_view(template_name='main.html'))),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
                       url(r'^api/', include('api.urls')),
                       url(r'^api-token-auth/', views.obtain_auth_token),
                       url(r'^accounts/', include('registration.backends.simple.urls')),
                       )
